questions.sort(sortQuestions);
    answers.sort(sortAnswersByQuestion);

    createHtml();

    function createHtml() {
        if(taken) {
            $('#quizLoc').html("");
            return;
        }

        html = "";
        for(var i = 0; i < questions.length; i++) {
            var id = questions[i].id;
            html += createQuestionDiv(i, questions[i], getAnswersForQuestion(id));
        }
        html += "<button type='submit' class='btn btn-success'>Submit</button>";
        $("#questionsLoc").html(html);
    }

    function createQuestionDiv(arrId, question, answers) {
        ans = getAnswersForQuestion(question.id);
        html = "<div class='row'><div class='col-md-6 offset-md-3'><p>";
        html += question.questionText;
        html += "</p>";

        for(var i = 0; i < answers.length; i++) {
            html += "<div class='form-check'><label class='form-check-label'><input name='answers[" + arrId + "].answer' class='form-check-input' type='checkbox' value='" + answers[i].id + "'>";
            html += answers[i].answerText;
            html += "</label></div>";
        }

        html += "<input type='hidden' name='answers[" + arrId + "].question' value='" + question.id + "'>";
        html += "</div></div>";
        return html;
    }

    function getAnswersForQuestion(questionId) {
        var array = [];
        for(var i = 0; i < answers.length; i++) {
            if(answers[i].questionId == questionId)
                array.push(answers[i]);
        }

        return array;
    }


    function sortQuestions(a,b) {
        if(a.questionOrder < b.questionOrder) {
            return -1;
        }
        if(a.questionOrder > b.questionOrder) {
            return 1;
        }
        return 0;
    }

    function sortAnswersByQuestion(a,b) {
        if(a.questionId < b.questionId)
            return -1;
        if(a.questionId > b.questionId)
            return 1;
        return 0;
    }

    //alert(answers[0].questionId);