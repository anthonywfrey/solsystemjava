/**
 * This is a custom exception which can be thrown should someone try to access something they are not authorized to have access to.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-26-2019
 */
package com.codigosoft.solsystem.exceptions;

public class InvalidAuthorizationException extends RuntimeException {

    public InvalidAuthorizationException() {
        super("Invalid authorization to perform this action");
    }
}
