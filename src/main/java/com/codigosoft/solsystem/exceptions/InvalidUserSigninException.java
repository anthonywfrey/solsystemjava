/**
 * This is the custom exception to throw whenever an invalid login is not attempted.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-7-2019
 */
package com.codigosoft.solsystem.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class InvalidUserSigninException extends RuntimeException{
    public InvalidUserSigninException() {
        super("Invalid login. The credentials do not match an existing user");
    }
}
