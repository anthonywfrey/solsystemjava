/**
 * This is a custom model that represents the body of an signin response
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-1-2019
 */
package com.codigosoft.solsystem.models.apimodels;

import java.io.Serializable;

public class SigninResponse implements Serializable{
    private String sessionToken;

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }
}
