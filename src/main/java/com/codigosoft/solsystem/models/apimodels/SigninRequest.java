/**
 * This is a custom model that represents the body of an signin request
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-1-2019
 */
package com.codigosoft.solsystem.models.apimodels;

import java.io.Serializable;

public class SigninRequest implements Serializable{
    private String tenantName;
    private String email;
    private String password;

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
