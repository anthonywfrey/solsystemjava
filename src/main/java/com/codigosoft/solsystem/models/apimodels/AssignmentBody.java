/**
 * This is a custom model that represents the body of an Assignment POST or PUT
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-22-2019
 */
package com.codigosoft.solsystem.models.apimodels;

import java.io.Serializable;

public class AssignmentBody implements Serializable {
    private String name;
    private String content;
    private Boolean isGraded;
    private Double maxPoints;
    private Long assignmentOrder;
    private Long sectionId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getIsGraded() {
        return isGraded;
    }

    public void setIsGraded(Boolean graded) {
        isGraded = graded;
    }

    public Double getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Double maxPoints) {
        this.maxPoints = maxPoints;
    }

    public Long getAssignmentOrder() {
        return assignmentOrder;
    }

    public void setAssignmentOrder(Long assignmentOrder) {
        this.assignmentOrder = assignmentOrder;
    }

    public Long getSectionId() {
        return sectionId;
    }

    public void setSectionId(Long sectionId) {
        this.sectionId = sectionId;
    }
}
