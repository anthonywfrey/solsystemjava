/**
 * This is a custom model that represents the body of an Question POST or PUT
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-22-2019
 */
package com.codigosoft.solsystem.models.apimodels;

import java.io.Serializable;

public class QuestionBody implements Serializable {
    private String questionText;
    private Long questionOrder;
    private Long assignmentId;

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public Long getQuestionOrder() {
        return questionOrder;
    }

    public void setQuestionOrder(Long questionOdrder) {
        this.questionOrder = questionOdrder;
    }

    public Long getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }
}
