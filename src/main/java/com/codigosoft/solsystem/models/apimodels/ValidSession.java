/**
 * This is a custom model that helps the system interact with (i.e. find, update, and check information) data related
 * to the REST API session. The biggest advantage to this model is it will update the session timeout for the provided user
 * when called, thus extending the timeout by the existing time of the request + 30 minutes.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-22-2019
 */
package com.codigosoft.solsystem.models.apimodels;

import com.codigosoft.solsystem.exceptions.InvalidUserSigninException;
import com.codigosoft.solsystem.models.Customer;
import com.codigosoft.solsystem.models.CustomerRole;
import com.codigosoft.solsystem.models.Role;
import com.codigosoft.solsystem.models.Tenant;
import com.codigosoft.solsystem.repositories.CustomerRepository;
import com.codigosoft.solsystem.repositories.CustomerRoleRepository;
import com.codigosoft.solsystem.repositories.RoleRepository;
import com.codigosoft.solsystem.repositories.TenantRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class ValidSession {
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    TenantRepository tenantRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    CustomerRoleRepository customerRoleRepository;

    private Customer customer;

    public ValidSession(String token, CustomerRepository customerRepository) {
        if(token.isEmpty()) {
            throw new InvalidUserSigninException();
        }
        this.customerRepository = customerRepository;
        List<Customer> customers = customerRepository.findBySessionToken(token);
        if(customers.size() <= 0) {
            throw new InvalidUserSigninException();
        }

        if(customers.get(0).getSessionTimeout().isBefore(LocalDateTime.now())) {
            throw new InvalidUserSigninException();
        }

        Customer updatedCustomer = customers.get(0);
        updatedCustomer.setSessionTimeout(LocalDateTime.now().plusMinutes(30));
        this.customer = customerRepository.save(updatedCustomer);
    }

    public Customer getCustomer() {
        return customer;
    }

    public void signOutSession() {
        this.customer.setSessionTimeout(LocalDateTime.now());
        customerRepository.save(this.customer);

        this.customer = null;
    }

    public String retrieveCustomerRoleName(CustomerRoleRepository customerRoleRepository) {
        if(this.customer == null) {
            throw new InvalidUserSigninException();
        }

        List<CustomerRole> customerRole = customerRoleRepository.findByCustomer(this.customer);
        return customerRole.get(0).getRole().getRoleName();
    }
}
