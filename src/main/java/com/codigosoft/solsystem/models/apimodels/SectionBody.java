/**
 * This is a custom model that represents the body of an Section POST or PUT
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-22-2019
 */
package com.codigosoft.solsystem.models.apimodels;

import java.io.Serializable;

public class SectionBody implements Serializable {
    private String name;
    private Long sectionOrder;
    private Long classroomId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSectionOrder() {
        return sectionOrder;
    }

    public void setSectionOrder(Long sectionOdrder) {
        this.sectionOrder = sectionOdrder;
    }

    public Long getClassroomId() {
        return classroomId;
    }

    public void setClassroomId(Long classroomId) {
        this.classroomId = classroomId;
    }
}
