/**
 * This is a custom model that represents the body of an Answer POST or PUT
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-22-2019
 */
package com.codigosoft.solsystem.models.apimodels;

import java.io.Serializable;

public class AnswerBody implements Serializable {
    private String answerText;
    private Long answerOrder;
    private Boolean correct;
    private Long questionId;

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public Long getAnswerOrder() {
        return answerOrder;
    }

    public void setAnswerOrder(Long answerOrder) {
        this.answerOrder = answerOrder;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }
}
