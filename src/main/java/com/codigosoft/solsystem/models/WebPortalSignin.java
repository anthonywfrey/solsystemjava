/**
 * This is the Web Portal model for a customer signin. This will allow for form input to authenticate the user.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-7-2019
 */
package com.codigosoft.solsystem.models;

public class WebPortalSignin {
    private String tenantSlug;
    private String customerEmail;
    private String customerPassword;

    public String getTenantSlug() {
        return tenantSlug;
    }

    public void setTenantSlug(String tenantSlug) {
        this.tenantSlug = tenantSlug;
    }

    public String getCustomerPassword() {
        return customerPassword;
    }

    public void setCustomerPassword(String customerPassword) {
        this.customerPassword = customerPassword;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }
}
