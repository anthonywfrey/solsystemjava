/**
 * This is a custom model designed to handle the POST made when a user submits the answers to their questions (note the array
 * format for answer. A user can submit multiple answers for questions that have more than one correct answer. If you remove
 * this array, the system will break). This contains a list of the question ids and every answer associated with it from the user.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-5-2019
 */
package com.codigosoft.solsystem.models;

import java.util.List;

public class FormQuestionAnswerCollection {
    private List<FormQuestionAnswer> answers;

    public List<FormQuestionAnswer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<FormQuestionAnswer> answers) {
        this.answers = answers;
    }
}
