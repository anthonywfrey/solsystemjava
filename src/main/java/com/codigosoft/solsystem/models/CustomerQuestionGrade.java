/**
 * This is a custom model designed to aid in the retrieval and display of a students answers on the questions of an assignment.
 * This is a highly inefficient model which I have simply designed to just work. This will be one area that might need to
 * be examined later on for efficiency improvements.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-15-2019
 */
package com.codigosoft.solsystem.models;

import com.codigosoft.solsystem.exceptions.ResourceNotFoundException;
import com.codigosoft.solsystem.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CustomerQuestionGrade {
    @Autowired
    CustomerAnswerRepository customerAnswerRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    AnswerRepository answerRepository;

    @Autowired
    AssignmentRepository assignmentRepository;

    private Customer customer;
    private HashMap<Long, ArrayList<Answer>> questionAnswers;
    private HashMap<Long, ArrayList<CustomerAnswer>> customerAnswers;
    private HashMap<Long, Boolean> isCorrect;
    private double score;

    public CustomerQuestionGrade() {

    }

    public CustomerQuestionGrade(Long customerId, Long assignmentId) {
        this.questionAnswers = new HashMap<>();
        this.customerAnswers = new HashMap<>();
        this.isCorrect = new HashMap<>();

        List<Question> qs = questionRepository.findByAssignmentId(assignmentId);
        List<Answer> qas = answerRepository.findByAssignmentId(assignmentId);
        List<CustomerAnswer> cas = customerAnswerRepository.findByCustomerIdAndAssignmentId(customerId, assignmentId);
        Assignment assignment = assignmentRepository.findById(assignmentId).orElseThrow(() -> new ResourceNotFoundException("assignment", "id", assignmentId));
        this.customer = customerRepository.findById(customerId).orElseThrow(() -> new ResourceNotFoundException("customer", "id", customerId));
        double tRight = 0.0;
        double tWrong = 0.0;

        for(int i = 0; i < qs.size(); i++) {
            Boolean correct = true;
            ArrayList<Answer> tempAnswers = new ArrayList<>();
            ArrayList<CustomerAnswer> tempCustomerAnswers = new ArrayList<>();

            for(int j = 0; j < cas.size(); j++) {
                if(cas.get(j).getQuestion().getId() == qs.get(i).getId()) {
                    tempCustomerAnswers.add(cas.get(j));
                }
                if(!cas.get(j).getIsCorrect()) {
                    correct = false;
                }
            }

            this.customerAnswers.put(qs.get(i).getId(), tempCustomerAnswers);
            this.isCorrect.put(qs.get(i).getId(), correct);
            if(correct) {
                tRight += 1;
            } else {
                tWrong += 1;
            }

            for(int k = 0; k < qas.size(); k++) {
                if(qas.get(k).getQuestion().getId() != qs.get(i).getId()) {
                    tempAnswers.add(qas.get(k));
                }
            }

            this.questionAnswers.put(qs.get(i).getId(), tempAnswers);
            this.score = (tRight/qs.size()) * assignment.getMaxPoints();
        }
    }
}
