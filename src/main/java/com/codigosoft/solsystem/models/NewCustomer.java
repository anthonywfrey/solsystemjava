/**
 * This is the API Model for a new Customer. We do not want the customer to pass up multiple models, so this simple Model
 * is used for JSON Deserialization of the expected API Calls. This model might not be used later on as it might be beneficial
 * to force the organization to signup via the web portal.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-7-2019
 */
package com.codigosoft.solsystem.models;

import java.io.Serializable;

public class NewCustomer implements Serializable {
    private String email;
    private String password;
    private Long tenantId;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }
}
