/**
 * This is the Customer Entity. It will represent the customers records in the database. It has a few extra methods for
 * dealing with password hashing.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-7-2019
 */
package com.codigosoft.solsystem.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;
import org.hibernate.type.TextType;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "customers")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"})
public class Customer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //The Properties
    @NotBlank
    @Email
    private String email;

    //@NotBlank
    @JsonIgnore
    @Type(type = "text")
    private String passwordDigest;

    @Transient
    private String password;

    private String sessionToken;

    private LocalDateTime sessionTimeout;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="tenant_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Tenant tenant;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public LocalDateTime getSessionTimeout() {
        return sessionTimeout;
    }

    public void setSessionTimeout(LocalDateTime sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }

    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @Transient
    public String getPassword() {
        return password;
    }


    public void setPasswordDigest(String passwordDigest) {
        this.passwordDigest = passwordDigest;
    }

    @JsonIgnore
    public String getPasswordDigest()
    {
        return this.passwordDigest;
    }

    @JsonIgnore
    public void setPassword(String password)
    {
        this.password = password;
        String hash = BCrypt.hashpw(password, BCrypt.gensalt());
        this.passwordDigest = hash;
    }

    @JsonIgnore
    public boolean passwordMatches(String password)
    {
        return BCrypt.checkpw(password, this.passwordDigest);
    }
}
