/**
 * This is a custom model  designed to represent the questions to an assignment. this will be used to pass the data directly to javascript in the view
 * so the view can manage the questions for displaying to the user.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-5-2019
 */
package com.codigosoft.solsystem.models;

import java.io.Serializable;

public class JSQuestion implements Serializable {

    private Long id;
    private String questionText;
    private Long questionOrder;
    private Long assignmentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public Long getQuestionOrder() {
        return questionOrder;
    }

    public void setQuestionOrder(Long questionOrder) {
        this.questionOrder = questionOrder;
    }

    public Long getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }
}
