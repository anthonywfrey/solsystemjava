/**
 * This is a custom model  designed to represent the answers to a question. this will be used to pass the data directly to javascript in the view
 * so the view can manage the answers to questions displayed to the user.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-5-2019
 */
package com.codigosoft.solsystem.models;

import java.io.Serializable;

public class JSAnswer implements Serializable {
    private Long id;
    private String answerText;
    private Long answerOrder;
    private Long questionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAnswerOrder() {
        return answerOrder;
    }

    public void setAnswerOrder(Long answerOrder) {
        this.answerOrder = answerOrder;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }
}
