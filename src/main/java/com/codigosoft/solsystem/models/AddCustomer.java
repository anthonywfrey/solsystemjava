/**
 * This is a custom form model. It is used to handle form input when an administrator adds a new customer.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-12-2019
 */
package com.codigosoft.solsystem.models;

public class AddCustomer {
    private String email;
    private String password;
    private Long roleId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
