/**
 * This is a custom model designed to handle the POST made when a user submits the answers to their questions (note the array
 * format for answer. A user can submit multiple answers for questions that have more than one correct answer. If you remove
 * this array, the system will break). Each question contains multiple answers. So this will be used in a collection model to process
 * the collection of questions and answers submitted at one time.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-5-2019
 */
package com.codigosoft.solsystem.models;

public class FormQuestionAnswer {
    private Long [] answer;
    private Long question;

    public Long[] getAnswer() {
        return answer;
    }

    public void setAnswer(Long[] answer) {
        this.answer = answer;
    }

    public Long getQuestion() {
        return question;
    }

    public void setQuestion(Long question) {
        this.question = question;
    }
}
