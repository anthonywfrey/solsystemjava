/**
 * This is a custom model designed to track the existing signed in user (via the web portal). Since a lot of the application
 * functions will revolve around checking the tenant or role of a customer. those models will be stored alongside the customer
 * in the session cookie using this model.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-11-2019
 */
package com.codigosoft.solsystem.models;

public class CustomerSession {
    private Customer customer;
    private Tenant tenant;
    private Role role;

    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Long getCustomerId() {
        return this.customer.getId();
    }

    public Long getTenantId() {
        return  this.tenant.getId();
    }

    public Long getRoleId() {
        return this.role.getId();
    }
}
