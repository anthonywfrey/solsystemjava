/**
 * This is the Web Portal model for a new organization signup. All new signups should create a new Tenant as well as the
 * first customer. The customer is the owner account created at tenant signup to allow for the creation of more records.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-7-2019
 */
package com.codigosoft.solsystem.models;

public class WebPortalSignup {
    private String tenantName;
    private String tenantSlug;
    private String customerEmail;
    private String customerPassword;
    private String customerPasswordConfirm;

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getTenantSlug() {
        return tenantSlug;
    }

    public void setTenantSlug(String tenantSlug) {
        this.tenantSlug = tenantSlug;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPassword() {
        return customerPassword;
    }

    public void setCustomerPassword(String customerPassword) {
        this.customerPassword = customerPassword;
    }

    public String getCustomerPasswordConfirm() {
        return customerPasswordConfirm;
    }

    public void setCustomerPasswordConfirm(String customerPasswordConfirm) {
        this.customerPasswordConfirm = customerPasswordConfirm;
    }
}
