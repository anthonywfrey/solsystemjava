package com.codigosoft.solsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class SolsystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(SolsystemApplication.class, args);
	}

}
