/**
 * This is the Section controller. It handles showing a list of the sections for a classroom (administration perspective).
 * It is also responsible for handling the requests for creating new sections, updating existing sections, and deleting sections.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-19-2019
 */
package com.codigosoft.solsystem.controllers;

import com.codigosoft.solsystem.models.Classroom;
import com.codigosoft.solsystem.models.CustomerSession;
import com.codigosoft.solsystem.models.Section;
import com.codigosoft.solsystem.repositories.ClassroomRepository;
import com.codigosoft.solsystem.repositories.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "classrooms/{id}/sections")
public class SectionController {
    @Autowired
    ClassroomRepository classroomRepository;

    @Autowired
    SectionRepository sectionRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView index(HttpSession session, @PathVariable(name = "id") Long classroomId) {
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");
        ModelAndView mv = new ModelAndView("Section/index");
        Classroom classroom = classroomRepository.findById(classroomId).orElse(new Classroom());

        if(classroom.getId() == null) {
            mv.addObject("error", "Classroom does not exist");
            return mv;
        }

        if(customerSession.getTenant().getId() != classroom.getTenant().getId()) {
            mv.addObject("error", "You are not the owner of this object");
            return mv;
        }

        List<Section> sections = sectionRepository.findByClassroom(classroom);

        if(sections.size() <= 0) {
            sections = new ArrayList<>();
        }
        mv.addObject("classroom", classroom);
        mv.addObject("sections", sections);

        return mv;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ModelAndView createSection(HttpSession session, @PathVariable(name = "id") Long classroomId
    , String txtCreateName, Long txtCreateOrder) {
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");
        ModelAndView mv = new ModelAndView("Section/index");
        Classroom classroom = classroomRepository.findById(classroomId).orElse(new Classroom());

        if(classroom.getId() == null) {
            mv.addObject("error", "Classroom does not exist");
            return mv;
        }

        if(customerSession.getTenant().getId() != classroom.getTenant().getId()) {
            mv.addObject("error", "You are not the owner of this object");
            return mv;
        }

        Section section = new Section();
        section.setClassroom(classroom);
        section.setTenant(customerSession.getTenant());

        section.setName(txtCreateName);
        section.setSectionOrder(txtCreateOrder);

        sectionRepository.save(section);

        List<Section> sections = sectionRepository.findByClassroom(classroom);

        if(sections.size() <= 0) {
            sections = new ArrayList<>();
        }

        mv.addObject("classroom", classroom);
        mv.addObject("success", "Section successfully saved.");
        mv.addObject("sections", sections);
        return mv;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ModelAndView updateSection(HttpSession session, @PathVariable(name = "id") Long classroomId, String txtUpdateName, Long txtUpdateOrder, Long txtUpdateId) {
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");
        ModelAndView mv = new ModelAndView("Section/index");
        Classroom classroom = classroomRepository.findById(classroomId).orElse(new Classroom());
        Section section = sectionRepository.findById(txtUpdateId).orElse(new Section());

        if(section.getId() == null) {
            mv.addObject("error", "Classroom does not exist");
            return mv;
        }

        if(classroom.getId() == null) {
            mv.addObject("error", "Classroom does not exist");
            return mv;
        }

        if(customerSession.getTenant().getId() != classroom.getTenant().getId()) {
            mv.addObject("error", "You are not the owner of this object");
            return mv;
        }

        section.setSectionOrder(txtUpdateOrder);
        section.setName(txtUpdateName);

        sectionRepository.save(section);

        List<Section> sections = sectionRepository.findByClassroom(classroom);

        if(sections.size() <= 0) {
            sections = new ArrayList<>();
        }

        mv.addObject("classroom", classroom);
        mv.addObject("success", "Section successfully saved.");
        mv.addObject("sections", sections);
        return mv;
    }

    @RequestMapping(value = "/{sectionid}", method = RequestMethod.DELETE)
    public ModelAndView deleteSection(HttpSession session, @PathVariable(name = "id") Long classroomId, @PathVariable(name = "sectionid") Long sectionId) {
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");
        ModelAndView mv = new ModelAndView("Section/index");
        Classroom classroom = classroomRepository.findById(classroomId).orElse(new Classroom());
        Section section = sectionRepository.findById(sectionId).orElse(new Section());

        if(section.getId() == null) {
            mv.addObject("error", "Classroom does not exist");
            return mv;
        }

        if(classroom.getId() == null) {
            mv.addObject("error", "Classroom does not exist");
            return mv;
        }

        if(customerSession.getTenant().getId() != classroom.getTenant().getId()) {
            mv.addObject("error", "You are not the owner of this object");
            return mv;
        }

        sectionRepository.deleteById(section.getId());
        List<Section> sections = sectionRepository.findByClassroom(classroom);

        if(sections.size() <= 0) {
            sections = new ArrayList<>();
        }

        mv.addObject("classroom", classroom);
        mv.addObject("success", "Section successfully saved.");
        mv.addObject("sections", sections);
        return mv;
    }
}
