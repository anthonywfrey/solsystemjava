/**
 * This is the Answer controller. It handles showing a list of the answers for a question (administration perspective).
 * It is also responsible for handling the requests for creating new answers, updating existing answers, and deleting answers.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-26-2019
 */
package com.codigosoft.solsystem.controllers;

import com.codigosoft.solsystem.models.Answer;
import com.codigosoft.solsystem.models.CustomerSession;
import com.codigosoft.solsystem.models.Question;
import com.codigosoft.solsystem.repositories.AnswerRepository;
import com.codigosoft.solsystem.repositories.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/questions/{questionid}/answers")
public class AnswerController {
    @Autowired
    AnswerRepository answerRepository;

    @Autowired
    QuestionRepository questionRepository;

    @RequestMapping(value="", method = RequestMethod.GET)
    public ModelAndView index(HttpSession session, @PathVariable(name = "questionid") Long questionId) {
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Question question = questionRepository.findById(questionId).orElse(new Question());
        ModelAndView mv = new ModelAndView("Answer/index");

        if(question.getId() == null) {
            mv.addObject("error", "Question does not exist");
            return mv;
        }

        if(question.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "You do not own this question");
            return mv;
        }

        List<Answer> answers = answerRepository.findByQuestion(question);
        if(answers.size() <= 0) {
            answers = new ArrayList<>();
        }
        mv.addObject("question", question);
        mv.addObject("answers", answers);
        return mv;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ModelAndView createAnswer(HttpSession session, @PathVariable(name = "questionid") Long questionId, String txtCreateText, Long txtCreateOrder, String chkCreateCorrect) {
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Question question = questionRepository.findById(questionId).orElse(new Question());
        ModelAndView mv = new ModelAndView("Answer/index");

        if(question.getId() == null) {
            mv.addObject("error", "Question does not exist");
            return mv;
        }

        if(question.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "You do not own this question");
            return mv;
        }

        Answer answer = new Answer();
        answer.setAnswerText(txtCreateText);
        answer.setAnswerOrder(txtCreateOrder);

        if(chkCreateCorrect == null) {
            answer.setCorrect(false);
        } else {
            answer.setCorrect(true);
        }

        answer.setQuestion(question);
        answer.setTenant(customerSession.getTenant());

        answerRepository.save(answer);

        List<Answer> answers = answerRepository.findByQuestion(question);
        if(answers.size() <= 0) {
            answers = new ArrayList<>();
        }

        mv.addObject("success", "Successfully added answer");
        mv.addObject("question", question);
        mv.addObject("answers", answers);
        return mv;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ModelAndView updateAnswer(HttpSession session, @PathVariable(name = "questionid") Long questionId, String txtUpdateText, Long txtUpdateOrder, String chkUpdateCorrect, Long txtUpdateId) {
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Question question = questionRepository.findById(questionId).orElse(new Question());
        Answer answer = answerRepository.findById(txtUpdateId).orElse(new Answer());
        ModelAndView mv = new ModelAndView("Answer/index");

        if(question.getId() == null) {
            mv.addObject("error", "Question does not exist");
            return mv;
        }

        if(question.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "You do not own this question");
            return mv;
        }

        if(answer.getId() == null) {
            mv.addObject("error", "Answer does not exist");
            return mv;
        }

        if(answer.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "You do not own this answer");
            return mv;
        }

        answer.setAnswerText(txtUpdateText);
        answer.setAnswerOrder(txtUpdateOrder);
        if(chkUpdateCorrect == null) {
            answer.setCorrect(false);
        } else {
            answer.setCorrect(true);
        }

        answerRepository.save(answer);

        List<Answer> answers = answerRepository.findByQuestion(question);
        if(answers.size() <= 0) {
            answers = new ArrayList<>();
        }

        mv.addObject("success", "Successfully added answer");
        mv.addObject("question", question);
        mv.addObject("answers", answers);
        return mv;
    }

    @RequestMapping(value = "/{answerid}", method = RequestMethod.DELETE)
    public ModelAndView deleteAnswer(HttpSession session, @PathVariable(name = "questionid") Long questionId, @PathVariable(name="answerid") Long answerId) {
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Question question = questionRepository.findById(questionId).orElse(new Question());
        Answer answer = answerRepository.findById(answerId).orElse(new Answer());
        ModelAndView mv = new ModelAndView("Answer/index");

        if(question.getId() == null) {
            mv.addObject("error", "Question does not exist");
            return mv;
        }

        if(question.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "You do not own this question");
            return mv;
        }

        if(answer.getId() == null) {
            mv.addObject("error", "Answer does not exist");
            return mv;
        }

        if(answer.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "You do not own this answer");
            return mv;
        }

        answerRepository.delete(answer);

        List<Answer> answers = answerRepository.findByQuestion(question);
        if(answers.size() <= 0) {
            answers = new ArrayList<>();
        }

        mv.addObject("success", "Successfully deleted answer");
        mv.addObject("question", question);
        mv.addObject("answers", answers);
        return mv;
    }
}
