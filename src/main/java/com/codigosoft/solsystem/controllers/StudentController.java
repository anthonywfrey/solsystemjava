/**
 * This is the student controller. This controller is responsible for providing students with the means to take their classes,
 * and answer any questions assigned to the classes.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-26-2019
 */
package com.codigosoft.solsystem.controllers;

import com.codigosoft.solsystem.exceptions.ResourceNotFoundException;
import com.codigosoft.solsystem.models.*;
import com.codigosoft.solsystem.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.ast.Assign;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/students")
public class StudentController {
    @Autowired
    ClassroomRepository classroomRepository;

    @Autowired
    SectionRepository sectionRepository;

    @Autowired
    AssignmentRepository assignmentRepository;

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    AnswerRepository answerRepository;

    @Autowired
    CustomerAnswerRepository customerAnswerRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView index(HttpSession session) {
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");
        List<Classroom> classrooms = classroomRepository.findByCustomerEnrollment(customerSession.getCustomer().getId());
        ModelAndView mv = new ModelAndView("Student/index");
        mv.addObject("classrooms", classrooms);
        return mv;
    }

    @RequestMapping(value="/{classroomid}", method = RequestMethod.GET)
    public ModelAndView viewSections(HttpSession session, @PathVariable(name = "classroomid")Long classroomId) {
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Classroom classroom = classroomRepository.findById(classroomId).orElseThrow(() -> new ResourceNotFoundException("classroom", "id", classroomId));
        List<Section> sections = sectionRepository.findByClassroom(classroom);

        ModelAndView mv = new ModelAndView("Student/sections");
        mv.addObject("sections", sections);
        mv.addObject("classroom", classroom);
        return mv;
    }

    @RequestMapping(value = "/sections/{sectionid}", method = RequestMethod.GET)
    public ModelAndView viewAssignments (HttpSession session, @PathVariable(name = "sectionid") Long sectionId) {
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Section section = sectionRepository.findById(sectionId).orElseThrow(() -> new ResourceNotFoundException("section", "id", sectionId));
        List<Assignment> assignments = assignmentRepository.findBySection(section);

        ModelAndView mv = new ModelAndView("Student/assignments");
        mv.addObject("section", section);
        mv.addObject("assignments", assignments);

        return mv;
    }

    @RequestMapping(value = "/assignments/{assignmentid}", method = RequestMethod.GET)
    public ModelAndView viewQuestions (HttpSession session, @PathVariable(name = "assignmentid") Long assignmentId) {
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Assignment assignment = assignmentRepository.findById(assignmentId).orElseThrow(() -> new ResourceNotFoundException("assignment", "id", assignmentId));
        List<Question> questions = questionRepository.findByAssignment(assignment);
        List<Answer> answers = answerRepository.findByAssignmentId(assignmentId);
        ArrayList<JSQuestion> jsQuestions = new ArrayList<>();
        ArrayList<JSAnswer> jsAnswers = new ArrayList<>();
        List<CustomerAnswer> existingAnswers = customerAnswerRepository.findByCustomerIdAndAssignmentId(customerSession.getCustomer().getId(), assignmentId);

        for(int i = 0; i < questions.size(); i++) {
            JSQuestion q = new JSQuestion();
            q.setId(questions.get(i).getId());
            q.setAssignmentId(questions.get(i).getAssignment().getId());
            q.setQuestionOrder(questions.get(i).getQuestionOrder());
            q.setQuestionText(questions.get(i).getQuestionText());

            jsQuestions.add(q);
        }

        for(int i = 0; i < answers.size(); i++) {
            JSAnswer a = new JSAnswer();
            a.setId(answers.get(i).getId());
            a.setQuestionId(answers.get(i).getQuestion().getId());
            a.setAnswerOrder(answers.get(i).getAnswerOrder());
            a.setAnswerText(answers.get(i).getAnswerText());

            jsAnswers.add(a);
        }

        boolean quizTaken = false;
        if(existingAnswers.size() > 0) {
            quizTaken = true;
        }

        ModelAndView mv = new ModelAndView("Student/questions");
        mv.addObject("jsanswers", jsAnswers);
        mv.addObject("jsquestions", jsQuestions);
        mv.addObject("quiz_taken", quizTaken);
        mv.addObject("questions", questions);
        mv.addObject("assignment", assignment);
        return mv;
    }

    @RequestMapping(value = "/assignments/{assignmentid}", method = RequestMethod.POST)
    public ModelAndView testSubmit(HttpSession session, FormQuestionAnswerCollection collection, @PathVariable(name = "assignmentid") Long assignmentId) {
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");
        List<Classroom> classrooms = classroomRepository.findByCustomerEnrollment(customerSession.getCustomer().getId());

        String res = "";

        for(int i = 0; i < collection.getAnswers().size(); i++) {
            res = res + "Question: " +  collection.getAnswers().get(i).getQuestion();
            final int iind = i;
            Question question = questionRepository.findById(collection.getAnswers().get(i).getQuestion()).orElseThrow(() -> new ResourceNotFoundException("question", "id", collection.getAnswers().get(iind).getQuestion()));

            for(int j = 0; j < collection.getAnswers().get(i).getAnswer().length; j++) {
                res = res + "Answer: " + collection.getAnswers().get(i).getAnswer()[j];
                final int jind = j;
                Answer answer = answerRepository.findById(collection.getAnswers().get(iind).getAnswer()[jind]).orElseThrow(() -> new ResourceNotFoundException("answer", "id", collection.getAnswers().get(iind).getAnswer()[jind]));

                CustomerAnswer customerAnswer = new CustomerAnswer();
                customerAnswer.setQuestion(question);
                customerAnswer.setAnswer(answer);
                customerAnswer.setCustomer(customerSession.getCustomer());
                customerAnswer.setTenant(customerSession.getTenant());

                customerAnswerRepository.save(customerAnswer);
            }
        }

        Assignment assignment = assignmentRepository.findById(assignmentId).orElseThrow(() -> new ResourceNotFoundException("assignment", "id", assignmentId));
        List<Question> questions = questionRepository.findByAssignment(assignment);
        List<Answer> answers = answerRepository.findByAssignmentId(assignmentId);
        ArrayList<JSQuestion> jsQuestions = new ArrayList<>();
        ArrayList<JSAnswer> jsAnswers = new ArrayList<>();

        for(int i = 0; i < questions.size(); i++) {
            JSQuestion q = new JSQuestion();
            q.setId(questions.get(i).getId());
            q.setAssignmentId(questions.get(i).getAssignment().getId());
            q.setQuestionOrder(questions.get(i).getQuestionOrder());
            q.setQuestionText(questions.get(i).getQuestionText());

            jsQuestions.add(q);
        }

        for(int i = 0; i < answers.size(); i++) {
            JSAnswer a = new JSAnswer();
            a.setId(answers.get(i).getId());
            a.setQuestionId(answers.get(i).getQuestion().getId());
            a.setAnswerOrder(answers.get(i).getAnswerOrder());
            a.setAnswerText(answers.get(i).getAnswerText());

            jsAnswers.add(a);
        }

        ModelAndView mv = new ModelAndView("Student/questions");
        mv.addObject("success", "Successfully submitted quiz");
        mv.addObject("jsanswers", jsAnswers);
        mv.addObject("jsquestions", jsQuestions);
        mv.addObject("questions", questions);
        mv.addObject("assignment", assignment);
        return mv;
    }
}
