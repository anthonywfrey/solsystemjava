/**
 * This is the instructor controller. It is repsonisble for providing all the interaction necessary for instructors, such as retrieving
 * the lists of classes they are assigned to, the students in those classes, the sections and assignments and more. This section
 * is unfinished and will likely be one of the first priorities should the project move forward next month.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-1-2019
 */
package com.codigosoft.solsystem.controllers;

import com.codigosoft.solsystem.exceptions.ResourceNotFoundException;
import com.codigosoft.solsystem.models.*;
import com.codigosoft.solsystem.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/instructors")
public class InstructorController {
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    ClassroomRepository classroomRepository;
    @Autowired
    SectionRepository sectionRepository;
    @Autowired
    AssignmentRepository assignmentRepository;
    @Autowired
    EnrollmentRepository enrollmentRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView index(HttpSession session) {
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");
        List<Classroom> classrooms = classroomRepository.findByCustomerEnrollment(customerSession.getCustomer().getId());
        //List<Classroom> classrooms = classroomRepository.findByTenant(customerSession.getTenant());
        /*List<Enrollment> enrollments = enrollmentRepository.findByCustomer(customerSession.getCustomer());
        if(enrollments.size() <= 0) {
            enrollments = new ArrayList<>();
        }*/

        ModelAndView mv = new ModelAndView("Instructor/index");
        mv.addObject("classrooms", classrooms);
        //mv.addObject("enrollments", enrollments);
        return mv;
    }

    @RequestMapping(value="/{classroomid}", method = RequestMethod.GET)
    public ModelAndView viewSections(HttpSession session, @PathVariable(name = "classroomid") Long classroomId) {
        Classroom classroom = classroomRepository.findById(classroomId).orElseThrow(() -> new ResourceNotFoundException("classroom", "id", classroomId));
        List<Section> sections = sectionRepository.findByClassroom(classroom);
        List<Customer> students = customerRepository.findStudentClassroomEnrollment(classroom.getId());


        ModelAndView mv = new ModelAndView("Instructor/sections");
        mv.addObject("classroom", classroom);
        mv.addObject("sections", sections );
        mv.addObject("students", students);
        return mv;
    }

    @RequestMapping(value = "/sections/{sectionid}", method = RequestMethod.GET)
    public ModelAndView viewAssignments(HttpSession session, @PathVariable(name = "sectionid") Long sectionId) {
        Section section = sectionRepository.findById(sectionId).orElseThrow(() -> new ResourceNotFoundException("section", "id", sectionId));
        List<Assignment> assignments = assignmentRepository.findBySection(section);
        List<Customer> students = customerRepository.findStudentClassroomEnrollment(section.getClassroom().getId());

        ModelAndView mv = new ModelAndView("Instructor/assignments");
        mv.addObject("section", section);
        mv.addObject("assignments", assignments);
        return mv;
    }

    @RequestMapping(value = "/assignments/{assignmentid}", method = RequestMethod.GET)
    public ModelAndView viewAssignment(HttpSession session, @PathVariable(name="assignmentid") Long assignmentId) {
        Assignment assignment = assignmentRepository.findById(assignmentId).orElseThrow(() -> new ResourceNotFoundException("assignment", "id", assignmentId));
        Section section = assignment.getSection();
        List<Customer> students = customerRepository.findStudentClassroomEnrollment(section.getClassroom().getId());
        ArrayList<CustomerQuestionGrade> grades = new ArrayList<>();

        for(int i = 0; i < students.size(); i++) {
            CustomerQuestionGrade grade = new CustomerQuestionGrade(students.get(i).getId(), assignmentId);
        }

        ModelAndView mv = new ModelAndView("Instructor/assignments");
        mv.addObject("section", section);
        mv.addObject("assignment", assignment);
        return mv;
    }
}
