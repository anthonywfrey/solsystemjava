/**
 * This is the Answer controller. It handles showing a list of the classrooms in the system (administration perspective).
 * It is also responsible for handling the requests for creating new classriins, updating existing classrooms, and deleting classrooms.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-14-2019
 */
package com.codigosoft.solsystem.controllers;

import com.codigosoft.solsystem.models.Classroom;
import com.codigosoft.solsystem.models.CustomerSession;
import com.codigosoft.solsystem.models.NewClassroom;
import com.codigosoft.solsystem.repositories.ClassroomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/classrooms")
public class ClassroomController {
    @Autowired
    ClassroomRepository classroomRepository;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView index(HttpSession session) {
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");
        List<Classroom> classrooms = classroomRepository.findByTenant(customerSession.getTenant());

        if(classrooms.size() <= 0) {
            classrooms = new ArrayList<>();
        }

        ModelAndView mv = new ModelAndView("Classroom/index");
        mv.addObject("classrooms", classrooms);
        return mv;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ModelAndView createClassroom(HttpSession session, String txtCreateName, String txtCreateDescription, Date txtCreateStartDate, Date txtCreateEndDate) {
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");

        Classroom classroom = new Classroom();
        classroom.setName(txtCreateName);
        classroom.setDescription(txtCreateDescription);
        classroom.setStartDate(txtCreateStartDate);
        classroom.setEndDate(txtCreateEndDate);
        classroom.setTenant(customerSession.getTenant());

        classroomRepository.save(classroom);

        ModelAndView mv = new ModelAndView("Classroom/index");

        List<Classroom> classrooms = classroomRepository.findByTenant(customerSession.getTenant());

        if(classrooms.size() <= 0) {
            classrooms = new ArrayList<>();
        }
        mv.addObject("classrooms", classrooms);
        mv.addObject("success", "Classroom created");
        return mv;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ModelAndView deleteClassroom(HttpSession session, @PathVariable(name = "id") Long classroomId) {
        ModelAndView mv = new ModelAndView("Classroom/index");
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");
        classroomRepository.deleteById(classroomId);
        List<Classroom> classrooms = classroomRepository.findByTenant(customerSession.getTenant());

        if(classrooms.size() <= 0) {
            classrooms = new ArrayList<>();
        }
        mv.addObject("classrooms", classrooms);
        mv.addObject("customer", (CustomerSession)session.getAttribute("customer"));
        mv.addObject("success", "Record deleted");

        return mv;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ModelAndView updateClassroom(HttpSession session, String txtUpdateName, String txtUpdateDescription, Date txtUpdateStartDate, Date txtUpdateEndDate, String txtUpdateId)
    {
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");
        List<Classroom> classrooms = classroomRepository.findByTenant(customerSession.getTenant());
        ModelAndView mv = new ModelAndView("Classroom/index");

        Classroom classroom = classroomRepository.findById(Long.parseLong(txtUpdateId)).orElse(new Classroom());

        if(classroom.getId() == null) {
            mv.addObject("error", "Classroom not found.");
            return mv;
        }

        if(customerSession.getTenant().getId() != classroom.getTenant().getId()) {
            mv.addObject("error", "The customer is not part of your organization");
            return mv;
        }

        classroom.setName(txtUpdateName);
        classroom.setDescription(txtUpdateDescription);
        classroom.setStartDate(txtUpdateStartDate);
        classroom.setEndDate(txtUpdateEndDate);

        classroomRepository.save(classroom);

        if(classrooms.size() <= 0) {
            classrooms = new ArrayList<>();
        }
        mv.addObject("classrooms", classrooms);
        return mv;
    }
}
