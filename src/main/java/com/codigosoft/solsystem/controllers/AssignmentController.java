/**
 * This is the Assignment controller. It handles showing a list of the assignments for a section (administration perspective).
 * It is also responsible for handling the requests for creating new assignments, updating existing assignments, and deleting assignments.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-19-2019
 */
package com.codigosoft.solsystem.controllers;

import com.codigosoft.solsystem.models.Assignment;
import com.codigosoft.solsystem.models.CustomerSession;
import com.codigosoft.solsystem.models.Section;
import com.codigosoft.solsystem.repositories.AssignmentRepository;
import com.codigosoft.solsystem.repositories.ClassroomRepository;
import com.codigosoft.solsystem.repositories.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/sections/{sectionid}/assignments")
public class AssignmentController {
    @Autowired
    AssignmentRepository assignmentRepository;

    @Autowired
    ClassroomRepository classroomRepository;

    @Autowired
    SectionRepository sectionRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView index(HttpSession session, @PathVariable(name = "sectionid") Long sectionId)
    {
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Section section = sectionRepository.findById(sectionId).orElse(new Section());

        ModelAndView mv = new ModelAndView("Assignment/index.html");

        if(section.getId() == null) {
            mv.addObject("error", "Section not found");
            return mv;
        }

        if(section.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "This section does not belong to you");
            return mv;
        }

        List<Assignment> assignments = assignmentRepository.findBySection(section);
        if(assignments.size() <= 0) {
            assignments = new ArrayList<>();
        }
        mv.addObject("section", section);
        mv.addObject("assignments", assignments);

        return mv;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ModelAndView createPost(HttpSession session, @PathVariable(name = "sectionid") Long sectionId, String txtCreateName, Long txtCreateOrder, String txtCreateContent, String chkCreateGradable, Double txtCreateMaxPoints) {
        ModelAndView mv = new ModelAndView("Assignment/index");
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Section section = sectionRepository.findById(sectionId).orElse(new Section());

        if(section.getId() == null) {
            mv.addObject("error", "Section not found");
            return mv;
        }

        if(section.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "This section does not belong to you");
            return mv;
        }

        Assignment assignment = new Assignment();
        assignment.setName(txtCreateName);
        assignment.setContent(txtCreateContent);
        assignment.setAssignmentOrder(txtCreateOrder);
        if(chkCreateGradable == null) {
            assignment.setIsGraded(false);
            assignment.setMaxPoints(0.0);
        } else {
            assignment.setIsGraded(true);
            assignment.setMaxPoints(txtCreateMaxPoints);
        }
        assignment.setTenant(customerSession.getTenant());
        assignment.setSection(section);

        assignmentRepository.save(assignment);


        List<Assignment> assignments = assignmentRepository.findBySection(section);
        if(assignments.size() <= 0) {
            assignments = new ArrayList<>();
        }
        mv.addObject("section", section);
        mv.addObject("assignments", assignments);
        mv.addObject("success", "Assignment successfully added");

        return mv;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ModelAndView updateAssignment(HttpSession session, @PathVariable(name = "sectionid") Long sectionId, String txtUpdateName, Long txtUpdateOrder, String txtCreateContent, String chkUpdateGradable, Double txtUpdateMaxPoints, Long txtUpdateId) {
        ModelAndView mv = new ModelAndView("Assignment/index");
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Section section = sectionRepository.findById(sectionId).orElse(new Section());
        Assignment assignment = assignmentRepository.findById(txtUpdateId).orElse(new Assignment());

        if(section.getId() == null) {
            mv.addObject("error", "Section not found");
            return mv;
        }

        if(section.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "This section does not belong to you");
            return mv;
        }

        if(assignment.getId() == null) {
            mv.addObject("error", "Assignment not found");
            return mv;
        }

        if(assignment.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "This assignment does not belong to you");
            return mv;
        }

        assignment.setName(txtUpdateName);
        assignment.setAssignmentOrder(txtUpdateOrder);

        if(chkUpdateGradable == null) {
            assignment.setIsGraded(false);
            assignment.setMaxPoints(0.0);
        } else {
            assignment.setIsGraded(true);
            assignment.setMaxPoints(txtUpdateMaxPoints);
        }

        assignmentRepository.save(assignment);

        List<Assignment> assignments = assignmentRepository.findBySection(section);
        if(assignments.size() <= 0) {
            assignments = new ArrayList<>();
        }
        mv.addObject("section", section);
        mv.addObject("assignments", assignments);
        mv.addObject("success", "Assignment successfully updated");

        return mv;
    }

    @RequestMapping(value = "/{assignmentid}", method = RequestMethod.DELETE)
    public ModelAndView deleteAssignment(HttpSession session, @PathVariable(name = "sectionid")Long sectionId, @PathVariable(name = "assignmentid") Long assignmentId) {
        ModelAndView mv = new ModelAndView("Assignment/index");
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Section section = sectionRepository.findById(sectionId).orElse(new Section());
        Assignment assignment = assignmentRepository.findById(assignmentId).orElse(new Assignment());

        if(section.getId() == null) {
            mv.addObject("error", "Section not found");
            return mv;
        }

        if(section.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "This section does not belong to you");
            return mv;
        }

        if(assignment.getId() == null) {
            mv.addObject("error", "Assignment not found");
            return mv;
        }

        if(assignment.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "This assignment does not belong to you");
            return mv;
        }

        assignmentRepository.delete(assignment);

        List<Assignment> assignments = assignmentRepository.findBySection(section);
        if(assignments.size() <= 0) {
            assignments = new ArrayList<>();
        }
        mv.addObject("section", section);
        mv.addObject("assignments", assignments);
        mv.addObject("success", "Assignment successfully deleted");

        return mv;
    }
}
