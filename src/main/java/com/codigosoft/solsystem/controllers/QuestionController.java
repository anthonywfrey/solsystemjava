/**
 * This is the Question controller. It handles showing a list of the questions for an assignment (administration perspective).
 * It is also responsible for handling the requests for creating new questions, updating existing questions, and deleting questions.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-26-2019
 */

package com.codigosoft.solsystem.controllers;

import com.codigosoft.solsystem.models.Assignment;
import com.codigosoft.solsystem.models.CustomerSession;
import com.codigosoft.solsystem.models.Question;
import com.codigosoft.solsystem.repositories.AssignmentRepository;
import com.codigosoft.solsystem.repositories.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/assignments/{assignmentid}/questions")
public class QuestionController {
    @Autowired
    AssignmentRepository assignmentRepository;

    @Autowired
    QuestionRepository questionRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView index(HttpSession session, @PathVariable(name = "assignmentid") Long assignmentId) {
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Assignment assignment = assignmentRepository.findById(assignmentId).orElse(new Assignment());
        ModelAndView mv = new ModelAndView("Question/index");

        if(assignment.getId() == null) {
            mv.addObject("error", "Assignment does not exist");
            return mv;
        }

        if(assignment.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "You are not the owner of this assignment");
            return mv;
        }

        List<Question> questions = questionRepository.findByAssignment(assignment);

        if(questions.size() <= 0) {
            questions = new ArrayList<>();
        }

        mv.addObject("assignment", assignment);
        mv.addObject("questions", questions);
        return mv;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ModelAndView createQuestion(HttpSession session, @PathVariable(name = "assignmentid") Long assignmentId, String txtCreateText, Long txtCreateOrder) {
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Assignment assignment = assignmentRepository.findById(assignmentId).orElse(new Assignment());
        ModelAndView mv = new ModelAndView("Question/index");

        if(assignment.getId() == null) {
            mv.addObject("error", "Assignment does not exist");
            return mv;
        }

        if(assignment.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "You are not the owner of this assignment");
            return mv;
        }

        Question question = new Question();
        question.setQuestionText(txtCreateText);
        question.setQuestionOrder(txtCreateOrder);
        question.setAssignment(assignment);
        question.setTenant(customerSession.getTenant());

        questionRepository.save(question);

        List<Question> questions = questionRepository.findByAssignment(assignment);

        if(questions.size() <= 0) {
            questions = new ArrayList<>();
        }

        mv.addObject("assignment", assignment);
        mv.addObject("questions", questions);
        mv.addObject("success", "Successfully created question");

        return mv;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ModelAndView updateQuestion(HttpSession session, @PathVariable(name = "assignmentid") Long assignmentId, Long txtUpdateId, String txtUpdateText, Long txtUpdateOrder) {
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Assignment assignment = assignmentRepository.findById(assignmentId).orElse(new Assignment());
        Question question = questionRepository.findById(txtUpdateId).orElse(new Question());
        ModelAndView mv = new ModelAndView("Question/index");

        if(assignment.getId() == null) {
            mv.addObject("error", "Assignment does not exist");
            return mv;
        }

        if(assignment.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "You are not the owner of this assignment");
            return mv;
        }

        if(question.getId() == null) {
            mv.addObject("error", "Question does not exist");
            return mv;
        }

        if(question.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "You are not the owner of this question");
            return mv;
        }

        question.setQuestionText(txtUpdateText);
        question.setQuestionOrder(txtUpdateOrder);

        questionRepository.save(question);

        List<Question> questions = questionRepository.findByAssignment(assignment);

        if(questions.size() <= 0) {
            questions = new ArrayList<>();
        }

        mv.addObject("assignment", assignment);
        mv.addObject("questions", questions);
        mv.addObject("success", "Successfully updated question");

        return mv;
    }

    @RequestMapping(value = "/{questionid}", method = RequestMethod.DELETE)
    public ModelAndView deleteQuestion(HttpSession session, @PathVariable(name = "assignmentid") Long assignmentId, @PathVariable(name="questionid") Long questionId) {
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Assignment assignment = assignmentRepository.findById(assignmentId).orElse(new Assignment());
        Question question = questionRepository.findById(questionId).orElse(new Question());
        ModelAndView mv = new ModelAndView("Question/index");

        if(assignment.getId() == null) {
            mv.addObject("error", "Assignment does not exist");
            return mv;
        }

        if(assignment.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "You are not the owner of this assignment");
            return mv;
        }

        if(question.getId() == null) {
            mv.addObject("error", "Question does not exist");
            return mv;
        }

        if(question.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "You are not the owner of this question");
            return mv;
        }

        questionRepository.deleteById(questionId);

        List<Question> questions = questionRepository.findByAssignment(assignment);

        if(questions.size() <= 0) {
            questions = new ArrayList<>();
        }

        mv.addObject("assignment", assignment);
        mv.addObject("questions", questions);
        mv.addObject("success", "Successfully deleted question");

        return mv;
    }
}
