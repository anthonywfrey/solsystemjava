/**
 * This is the dadshboard controller. It will eventually contain the code that provides the UI and means of navigation for the administrator
 * dashboard. This is extremely basic and will be finished in the future (possibly).
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-14-2019
 */
package com.codigosoft.solsystem.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/dashboard")
public class DashboardController {

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView index(HttpSession session) {
        ModelAndView mv = new ModelAndView("Dashboard/index");

        return mv;
    }
}
