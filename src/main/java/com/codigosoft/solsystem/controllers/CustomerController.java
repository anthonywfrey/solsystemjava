/**
 * This is the Answer controller. It handles showing a list of the customers for the system (administration perspective).
 * It is also responsible for handling the requests for creating new customers, updating existing customers, and deleting customers.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-12-2019
 */
package com.codigosoft.solsystem.controllers;

import com.codigosoft.solsystem.models.*;
import com.codigosoft.solsystem.repositories.CustomerRepository;
import com.codigosoft.solsystem.repositories.CustomerRoleRepository;
import com.codigosoft.solsystem.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/customers")
public class CustomerController {
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    CustomerRoleRepository customerRoleRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView index(HttpSession session) {
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");

        List<Customer> customers = customerRepository.findByTenant(customerSession.getTenant());
        List<Role> roles = roleRepository.findByTenant(customerSession.getTenant());

        if (customers == null) {
            customers = new ArrayList<>();
        }

        ModelAndView mv = new ModelAndView("Customer/index");
        mv.addObject("customers", customers);
        mv.addObject("roles", roles);
        return mv;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ModelAndView addCustomer(@Valid AddCustomer addCustomerDetails, HttpSession session) {
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");

        Role role = roleRepository.findById(addCustomerDetails.getRoleId()).orElse(new Role());

        Customer customer = new Customer();
        customer.setEmail(addCustomerDetails.getEmail());
        customer.setPassword(addCustomerDetails.getPassword());
        customer.setTenant(customerSession.getTenant());

        Customer savedCustomer = customerRepository.save(customer);

        CustomerRole customerRole = new CustomerRole();
        customerRole.setCustomer(savedCustomer);
        customerRole.setRole(role);

        customerRoleRepository.save(customerRole);

        List<Customer> customers = customerRepository.findAll();

        if(customers == null)
        {
            customers = new ArrayList<>();
        }

        List<Role> roles = roleRepository.findByTenant(customerSession.getTenant());

        ModelAndView mv = new ModelAndView("Customer/index");
        mv.addObject("success", "Successfully added record");
        mv.addObject("customers", customers);
        mv.addObject("roles", roles);
        return mv;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ModelAndView deleteCustomer(HttpSession session, @PathVariable(name = "id") Long customerId) {
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");
        customerRepository.deleteById(customerId);

        List<Customer> customers = customerRepository.findAll();

        if(customers == null)
        {
            customers = new ArrayList<>();
        }
        List<Role> roles = roleRepository.findByTenant(customerSession.getTenant());
        ModelAndView mv = new ModelAndView("Customer/index");
        mv.addObject("success", "Record deleted.");
        mv.addObject("customers", customers);
        mv.addObject("roles", roles);
        return mv;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ModelAndView updateCustomer(HttpSession session, String updateEmail, String updatePassword, String updateId) {
        CustomerSession customerSession = (CustomerSession) session.getAttribute("customer");
        Customer customer = customerRepository.findById(Long.parseLong(updateId)).orElse(new Customer());
        ModelAndView mv = new ModelAndView("Customer/index");

        if(customer.getId() == null) {
            mv.addObject("error", "Customer not found.");
            return mv;
        }

        if(customer.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "Customer does not belong to this tenant.");
            return mv;
        }

        customer.setEmail(updateEmail);

        if(!updatePassword.isEmpty()) {
            customer.setPassword(updatePassword);
        }

        customerRepository.save(customer);

        List<Customer> customers = customerRepository.findAll();
        if(customers == null) {
            customers = new ArrayList<>();
        }
        List<Role> roles = roleRepository.findByTenant(customerSession.getTenant());
        mv.addObject("customers", customers);
        mv.addObject("roles", roles);
        mv.addObject("success", updatePassword);
        return mv;
    }
}
