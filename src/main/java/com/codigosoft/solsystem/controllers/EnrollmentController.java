/**
 * This is the Enrollment controller. It provides all the code necessary for enrolling students and instructors into courses.
 * Without this controller, there can be little end-user interaction for the system.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-29-2019
 */
package com.codigosoft.solsystem.controllers;

import com.codigosoft.solsystem.models.*;
import com.codigosoft.solsystem.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/enrollments")
public class EnrollmentController {
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    ClassroomRepository classroomRepository;

    @Autowired
    CustomerRoleRepository customerRoleRepository;

    @Autowired
    EnrollmentRepository enrollmentRepository;

    @Autowired
    RoleRepository roleRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView index(HttpSession session) {
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");

        boolean hasAdmin = false;
        if(customerSession.getRole().getRoleName().equals("Administrator") || customerSession.getRole().getRoleName().equals("Owner")) {
            hasAdmin = true;
        }

        if(!hasAdmin) {
            //error out here
        }

        ModelAndView mv = new ModelAndView("Enrollment/index");
        List<Classroom> classrooms = classroomRepository.findByTenant(customerSession.getTenant());

        if(classrooms.size() <= 0) {
            classrooms = new ArrayList<>();
        }

        mv.addObject("classrooms", classrooms);
        return mv;
    }

    @RequestMapping(value = "/{classroomid}", method = RequestMethod.GET)
    public ModelAndView enrollment(HttpSession session, @PathVariable(name = "classroomid") Long classroomId) {
        ModelAndView mv = new ModelAndView("Enrollment/enrollment");
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");
        Classroom classroom = classroomRepository.findById(classroomId).orElse(new Classroom());

        boolean hasAdmin = false;
        if(customerSession.getRole().getRoleName().equals("Administrator") || customerSession.getRole().getRoleName().equals("Owner")) {
            hasAdmin = true;
        }

        if(!hasAdmin) {
            mv.addObject("error", "You do not have the right privellages for this section");
            return mv;
        }

        if(classroom.getId() == null) {
            mv.addObject("error", "Classroom does not exist");
            return mv;
        }

        if(classroom.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "Classroom does not belong to you");
            return mv;
        }

        //List<Customer> enrollments = customerRepository.findByClassroomEnrollment(classroomId);
        List<Enrollment> enrollments = enrollmentRepository.findByClassroom(classroom);
        List<Customer> instructors = customerRepository.findInstructors();
        List<Customer> students = customerRepository.findStudents();

        if(instructors.size() <= 0) {
            instructors = new ArrayList<>();
        }

        if(students.size() <= 0) {
            students = new ArrayList<>();
        }

        if(enrollments.size() <= 0) {
            enrollments = new ArrayList<>();
        }

        mv.addObject("enrollments", enrollments);
        mv.addObject("classroom", classroom);
        mv.addObject("instructors", instructors);
        mv.addObject("students", students);


        return mv;
    }

    @RequestMapping(value = "/{classroomid}/customers/{customerid}", method = RequestMethod.POST)
    public ModelAndView enrollCustomer(HttpSession session, @PathVariable(name = "classroomid") Long classroomId, @PathVariable(name="customerid") Long customerId) {
        ModelAndView mv = new ModelAndView("Enrollment/enrollment");
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");
        Classroom classroom = classroomRepository.findById(classroomId).orElse(new Classroom());
        Customer customer = customerRepository.findById(customerId).orElse(new Customer());

        boolean hasAdmin = false;
        if(customerSession.getRole().getRoleName().equals("Administrator") || customerSession.getRole().getRoleName().equals("Owner")) {
            hasAdmin = true;
        }

        if(!hasAdmin) {
            mv.addObject("error", "You do not have the right privellages for this section");
            return mv;
        }

        if(classroom.getId() == null) {
            mv.addObject("error", "Classroom does not exist");
            return mv;
        }

        if(classroom.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "Classroom does not belong to you");
            return mv;
        }

        if(customer.getId() == null) {
            mv.addObject("error", "Customer does not exist");
            return mv;
        }

        if(customer.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "Customer does not belong to you");
            return mv;
        }

        List<CustomerRole> customerRoles = customerRoleRepository.findByCustomer(customer);

        if(customerRoles.size() <= 0) {
            mv.addObject("error", "Customer does not have a role");
            return mv;
        }

        CustomerRole customerRole = customerRoles.get(0);

        Role role = customerRole.getRole();

        if(!role.getRoleName().equals("Student") && !role.getRoleName().equals("Instructor")) {
            mv.addObject("error", "You can only register students or instructors for class");
            return  mv;
        }

        Enrollment enrollment = new Enrollment();
        enrollment.setClassroom(classroom);
        enrollment.setCustomer(customer);
        enrollment.setRole(role);
        enrollment.setTenant(customerSession.getTenant());
        enrollmentRepository.save(enrollment);

        //List<Customer> enrollments = customerRepository.findByClassroomEnrollment(classroomId);
        List<Enrollment> enrollments = enrollmentRepository.findByClassroom(classroom);
        List<Customer> instructors = customerRepository.findInstructors();
        List<Customer> students = customerRepository.findStudents();

        if(instructors.size() <= 0) {
            instructors = new ArrayList<>();
        }

        if(students.size() <= 0) {
            students = new ArrayList<>();
        }

        if(enrollments.size() <= 0) {
            enrollments = new ArrayList<>();
        }

        mv.addObject("enrollments", enrollments);
        mv.addObject("classroom", classroom);
        mv.addObject("instructors", instructors);
        mv.addObject("students", students);


        return mv;
    }

    @RequestMapping(value = "/{classroomid}/enrollments/{enrollmentid}", method = RequestMethod.DELETE)
    public ModelAndView unenrollCustomer(HttpSession session, @PathVariable(name = "classroomid") Long classroomId, @PathVariable(name="enrollmentid") Long enrollmentId) {
        ModelAndView mv = new ModelAndView("Enrollment/enrollment");
        CustomerSession customerSession = (CustomerSession)session.getAttribute("customer");
        Classroom classroom = classroomRepository.findById(classroomId).orElse(new Classroom());
        Enrollment enrollment = enrollmentRepository.findById(enrollmentId).orElse(new Enrollment());
        boolean hasAdmin = false;
        if(customerSession.getRole().getRoleName().equals("Administrator") || customerSession.getRole().getRoleName().equals("Owner")) {
            hasAdmin = true;
        }

        if(!hasAdmin) {
            mv.addObject("error", "You do not have the right privellages for this section");
            return mv;
        }

        if(classroom.getId() == null) {
            mv.addObject("error", "Classroom does not exist");
            return mv;
        }

        if(classroom.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "Classroom does not belong to you");
            return mv;
        }

        if(enrollment.getId() == null) {
            mv.addObject("error", "Enrollment does not exist");
            return mv;
        }

        if(enrollment.getTenant().getId() != customerSession.getTenant().getId()) {
            mv.addObject("error", "Enrollment does not belong to you");
            return mv;
        }

        enrollmentRepository.delete(enrollment);

        //List<Customer> enrollments = customerRepository.findByClassroomEnrollment(classroomId);
        List<Enrollment> enrollments = enrollmentRepository.findByClassroom(classroom);
        List<Customer> instructors = customerRepository.findInstructors();
        List<Customer> students = customerRepository.findStudents();

        if(instructors.size() <= 0) {
            instructors = new ArrayList<>();
        }

        if(students.size() <= 0) {
            students = new ArrayList<>();
        }

        if(enrollments.size() <= 0) {
            enrollments = new ArrayList<>();
        }

        mv.addObject("enrollments", enrollments);
        mv.addObject("classroom", classroom);
        mv.addObject("instructors", instructors);
        mv.addObject("students", students);


        return mv;
    }
}
