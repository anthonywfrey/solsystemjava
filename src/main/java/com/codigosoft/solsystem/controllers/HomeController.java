/**
 * This is the Home Controller for the basic web portal. It will handle all the display pages for the eventual website
 * (not the application), as well as the simple signup and signin forms. It will also handle the signout process.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-7-2019
 */
package com.codigosoft.solsystem.controllers;

import com.codigosoft.solsystem.models.*;
import com.codigosoft.solsystem.repositories.CustomerRepository;
import com.codigosoft.solsystem.repositories.CustomerRoleRepository;
import com.codigosoft.solsystem.repositories.RoleRepository;
import com.codigosoft.solsystem.repositories.TenantRepository;
import com.github.slugify.Slugify;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

@Controller
public class HomeController {
    @Autowired
    TenantRepository tenantRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    CustomerRoleRepository customerRoleRepository;

    @RequestMapping(value = "/")
    public ModelAndView index(HttpSession session)
    {
        ModelAndView home = new ModelAndView("Home/index");
        home.addObject("msg", "hello World");
        home.addObject("title", "Title");

        if(session.getAttribute("customer") != null) {
            home.addObject("customer", session.getAttribute("customer"));
        }

        return  home;
    }

    @RequestMapping(value = "/signup")
    public ModelAndView showSignupForm()
    {
        ModelAndView showForm = new ModelAndView("Home/signup");

        return showForm;
    }

    @RequestMapping(value="/signup", method = RequestMethod.POST, consumes = "application/x-www-form-urlencoded;charset=UTF-8")
    public ModelAndView signupFormSubmit(@Valid WebPortalSignup signupDetails) {
        ModelAndView mv = new ModelAndView("Home/signup");
        Slugify slugify = new Slugify();
        String slug = slugify.slugify(signupDetails.getTenantSlug());

        if (signupDetails.getTenantName().isEmpty() || signupDetails.getTenantSlug().isEmpty()
                || signupDetails.getCustomerEmail().isEmpty() || signupDetails.getCustomerPassword().isEmpty()
                || signupDetails.getCustomerPasswordConfirm().isEmpty()) {
            mv.addObject("error", "Please ensure that all fields are filled");
            return mv;
        }

        if(!signupDetails.getCustomerPassword().equals(signupDetails.getCustomerPasswordConfirm())) {
            mv.addObject("error", "Your passwords do not match");
            return mv;
        }

        if(!slug.equals(signupDetails.getTenantSlug())) {
            mv.addObject("error", "You must enter a valid slug.");
            return mv;
        }

        List<Tenant> tenants = tenantRepository.findByNameOrSlug(signupDetails.getTenantName(), signupDetails.getTenantSlug());

        if(tenants.size() > 0) {
            mv.addObject("error", "This tenant name already exists. Adjust this field and try again");
            return mv;
        }

        Tenant tenant = new Tenant();
        Customer customer = new Customer();
        Role role = new Role();
        Role role2 = new Role();
        Role role3 = new Role();
        Role role4 = new Role();

        tenant.setName(signupDetails.getTenantName());
        tenant.setSlug(slug);

        Tenant savedTenant = tenantRepository.save(tenant);

        customer.setEmail(signupDetails.getCustomerEmail());
        customer.setPassword(signupDetails.getCustomerPassword());
        customer.setTenant(savedTenant);

        Customer savedCustomer = customerRepository.save(customer);

        role.setTenant(savedTenant);
        role.setRoleName("Owner");

        role2.setTenant(savedTenant);
        role2.setRoleName("Administrator");

        role3.setTenant(savedTenant);
        role3.setRoleName("Instructor");

        role4.setTenant(savedTenant);
        role4.setRoleName("Student");

        Role savedRole = roleRepository.save(role);
        roleRepository.save(role2);
        roleRepository.save(role3);
        roleRepository.save(role4);

        CustomerRole customerRole = new CustomerRole();
        customerRole.setRole(savedRole);
        customerRole.setCustomer(savedCustomer);
        customerRoleRepository.save(customerRole);


        mv.addObject("error", "Succesfully created account.");
        return mv;
    }


    @RequestMapping(value = "/signin")
    public ModelAndView showSigninForm() {
        ModelAndView showSigninForm = new ModelAndView("Home/signin");

        return showSigninForm;
    }

    @RequestMapping(value="/signin", method = RequestMethod.POST)
    public ModelAndView signupFormSubmit(@Valid WebPortalSignin signinDetails, HttpSession session) {
        ModelAndView mv = new ModelAndView("Home/index");
        List<Tenant> tenantResults = tenantRepository.findBySlug(signinDetails.getTenantSlug());

        if(tenantResults.size() <= 0) {
            mv.addObject("error", "Account not found. Please check your details and try again.");
            return mv;
        }

        List<Customer> customerResults = customerRepository.findByEmailAndTenant(signinDetails.getCustomerEmail(), tenantResults.get(0));

        if(customerResults.size() <= 0){
            mv.addObject("error", "Account not found. Please check your details and try again.");
            return mv;
        }

        if(!BCrypt.checkpw(signinDetails.getCustomerPassword(), customerResults.get(0).getPasswordDigest())) {
            mv.addObject("error", "Account not found. Please check your details and try again.");
            return mv;
        }

        List<CustomerRole> customerRoles = customerRoleRepository.findByCustomer(customerResults.get(0));

        if(customerRoles.size() <= 0) {
            mv.addObject("error", "There is a problem with your account, please contact an administrator");
            return mv;
        }

        Role role = roleRepository.findById(customerRoles.get(0).getId()).orElse(new Role());

        if(role.getId() == null) {
            mv.addObject("error", "There is a problem with your account, please contact an administrator");
            return mv;
        }

        CustomerSession customerSession = new CustomerSession();
        customerSession.setCustomer(customerResults.get(0));
        customerSession.setTenant(tenantResults.get(0));
        customerSession.setRole(role);

        session.setAttribute("customer", customerSession);

        mv.addObject("success", "Successfully signed in.");
        mv.addObject("customer", session.getAttribute("customer"));

        return mv;
    }

    public ModelAndView signout(HttpSession session) {
        ModelAndView mv = new ModelAndView("Home/index");

        if(session.getAttribute("customer") != null) {
            session.removeAttribute("customer");
            mv.addObject("success", "Successfully signed out.");
        }

        return mv;
    }
}
