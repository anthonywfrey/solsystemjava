/**
 * This is the API controller for the user sessions. This controller is responsible for taking a POST request, getting the
 * email, and password, authenticating the user, generating a session token, and returning it to the calling user. It also
 * has a delete mapping for signing out a user.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-4-2019
 */
package com.codigosoft.solsystem.api.v1;

import com.codigosoft.solsystem.exceptions.InvalidUserSigninException;
import com.codigosoft.solsystem.exceptions.ResourceNotFoundException;
import com.codigosoft.solsystem.models.Customer;
import com.codigosoft.solsystem.models.Tenant;
import com.codigosoft.solsystem.models.apimodels.SigninRequest;
import com.codigosoft.solsystem.models.apimodels.SigninResponse;
import com.codigosoft.solsystem.repositories.CustomerRepository;
import com.codigosoft.solsystem.repositories.TenantRepository;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/session")
public class SessionApiController {
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    TenantRepository tenantRepository;

    @PostMapping("")
    public SigninResponse signin(@RequestBody SigninRequest signinRequest) {
        SigninResponse signinResponse = new SigninResponse();
        List<Tenant> tenant = tenantRepository.findBySlug(signinRequest.getTenantName());

        if(tenant.size() <= 0) {
            throw new ResourceNotFoundException("tenant", "slug", signinRequest.getTenantName());
        }

        List<Customer> customers = customerRepository.findByEmailAndTenant(signinRequest.getEmail(), tenant.get(0));
        if(customers.size() <= 0) {
            throw new ResourceNotFoundException("customer", "email", signinRequest.getEmail());
        }

        Customer customer = customers.get(0);
        if(!BCrypt.checkpw(signinRequest.getPassword(), customer.getPasswordDigest())) {
            throw new InvalidUserSigninException();
        }

        String token = UUID.randomUUID().toString();
        List<Customer> customersWithToken = customerRepository.findBySessionToken(token);
        boolean exists = true;

        while(exists) {
            if(customersWithToken.size() > 0) {
                token = UUID.randomUUID().toString();
                customersWithToken = customerRepository.findBySessionToken(token);
            }
            else {
                exists = false;
            }
        }

        customer.setSessionToken(token);
        LocalDateTime sessionTimeout = LocalDateTime.now();
        customer.setSessionTimeout(sessionTimeout.plusMinutes(30));
        customerRepository.save(customer);

        signinResponse.setSessionToken(customer.getSessionToken());
        return signinResponse;
    }

    @DeleteMapping("")
    public HashMap<String, String> signout (@RequestHeader(value = "SessionToken") String sessionToken){
        HashMap<String, String> response = new HashMap<>();
        //response.put("token", sessionToken);

        return response;
    }
}
