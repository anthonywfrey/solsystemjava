/**
 * This is the API controller for the CustomerAnswers model. This will allow for the creating retrieving, updating and deleting
 * the model via REST.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-15-2019
 */
package com.codigosoft.solsystem.api.v1;

import com.codigosoft.solsystem.exceptions.InvalidAuthorizationException;
import com.codigosoft.solsystem.exceptions.ResourceNotFoundException;
import com.codigosoft.solsystem.models.Answer;
import com.codigosoft.solsystem.models.CustomerAnswer;
import com.codigosoft.solsystem.models.apimodels.ValidSession;
import com.codigosoft.solsystem.repositories.AnswerRepository;
import com.codigosoft.solsystem.repositories.CustomerAnswerRepository;
import com.codigosoft.solsystem.repositories.CustomerRepository;
import com.codigosoft.solsystem.repositories.CustomerRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/customeranswers")
public class CustomerAnswerApiController {
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    AnswerRepository answerRepository;

    @Autowired
    CustomerAnswerRepository customerAnswerRepository;

    @Autowired
    CustomerRoleRepository customerRoleRepository;


    @PostMapping("")
    public CustomerAnswer createCustomerAnswer(@RequestHeader(value = "SessionToken")String sessionToken,  @Valid @RequestBody CustomerAnswer customerAnswerDetails){
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean canInsert = false;
        if(role.equals("Student")) {
            canInsert = true;
        }

        if(!canInsert) {
            throw new InvalidAuthorizationException();
        }

        customerAnswerDetails.setTenant(validSession.getCustomer().getTenant());
        return customerAnswerRepository.save(customerAnswerDetails);
    }

    @PutMapping("/{customeranswerid}")
    public CustomerAnswer updateCustomerAnswer(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "customeranswerid")Long customerAnswerId, @Valid @RequestBody CustomerAnswer customerAnswerDetails) {
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean canInsert = false;
        if(role.equals("Student")) {
            canInsert = true;
        }

        if(!canInsert) {
            throw new InvalidAuthorizationException();
        }

        CustomerAnswer customerAnswer = customerAnswerRepository.findById(customerAnswerId).orElseThrow(() -> new ResourceNotFoundException("customer_answer", "id", customerAnswerId));
        if(customerAnswer.getCustomer().getId() != validSession.getCustomer().getId()) {
            throw new InvalidAuthorizationException();
        }
        Answer answer = answerRepository.findById(customerAnswer.getAnswer().getId()).orElseThrow(() -> new ResourceNotFoundException("answer", "id", customerAnswer.getAnswer().getId()));
        customerAnswer.setQuestion(customerAnswerDetails.getQuestion());
        customerAnswer.setAnswer(customerAnswerDetails.getAnswer());
        customerAnswer.setIsCorrect(answer.getCorrect());

        return customerAnswerRepository.save(customerAnswer);
    }

    @DeleteMapping("/{customeranswerid}")
    public ResponseEntity<?> deleteCustomerAnser(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name="customeranswerid") Long customerAnswerId) {
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean canInsert = false;
        if(role.equals("Student")) {
            canInsert = true;
        }

        if(!canInsert) {
            throw new InvalidAuthorizationException();
        }

        CustomerAnswer customerAnswer = customerAnswerRepository.findById(customerAnswerId).orElseThrow(() -> new ResourceNotFoundException("customer_answer", "id", customerAnswerId));
        if(customerAnswer.getCustomer().getId() != validSession.getCustomer().getId()) {
            throw new InvalidAuthorizationException();
        }

        customerAnswerRepository.delete(customerAnswer);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/student/{studentid}/assignment/{assignmentid}")
    public List<CustomerAnswer> getCustomerAnswers(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "studentid") Long studentId, @PathVariable(name = "assignmentid") Long assignmentId) {
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean canGet = false;
        if(role.equals("Instructor")) {
            canGet = true;
        }

        if(!canGet) {
            throw new InvalidAuthorizationException();
        }

        List<CustomerAnswer> customerAnswers = customerAnswerRepository.findByCustomerIdAndAssignmentId(studentId, assignmentId);

        return customerAnswers;
    }
}
