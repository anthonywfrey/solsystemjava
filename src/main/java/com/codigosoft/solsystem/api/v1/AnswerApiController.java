/**
 * This is the API controller for the Answers model. This will allow for the creating retrieving, updating and deleting
 * the model via REST.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-8-2019
 */
package com.codigosoft.solsystem.api.v1;

import com.codigosoft.solsystem.exceptions.InvalidAuthorizationException;
import com.codigosoft.solsystem.exceptions.ResourceNotFoundException;
import com.codigosoft.solsystem.models.Answer;
import com.codigosoft.solsystem.models.Assignment;
import com.codigosoft.solsystem.models.Question;
import com.codigosoft.solsystem.models.apimodels.AnswerBody;
import com.codigosoft.solsystem.models.apimodels.ValidSession;
import com.codigosoft.solsystem.repositories.AnswerRepository;
import com.codigosoft.solsystem.repositories.CustomerRepository;
import com.codigosoft.solsystem.repositories.CustomerRoleRepository;
import com.codigosoft.solsystem.repositories.QuestionRepository;
import org.omg.CORBA.DynAnyPackage.Invalid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/answers")
public class AnswerApiController {
    //Autowired repositories
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CustomerRoleRepository customerRoleRepository;

    @Autowired
    AnswerRepository answerRepository;

    @Autowired
    QuestionRepository questionRepository;

    //Create Method
    @PostMapping("")
    public Answer createAnswer(@RequestHeader(value = "SessionToken") String sessionToken, @Valid @RequestBody AnswerBody answerDetails) {
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);
        Question question = questionRepository.findById(answerDetails.getQuestionId()).orElseThrow(() -> new ResourceNotFoundException("question", "id", answerDetails.getQuestionId()));

        boolean canUpdate = false;
        if(role.equals("Administrator") || role.equals("Owner"))
            canUpdate = true;

        if(!canUpdate)
            throw new InvalidAuthorizationException();

        Answer answer = new Answer();
        answer.setAnswerText(answerDetails.getAnswerText());
        answer.setAnswerOrder(answerDetails.getAnswerOrder());
        answer.setCorrect(answerDetails.getCorrect());
        answer.setQuestion(question);
        answer.setTenant(validSession.getCustomer().getTenant());

        return answerRepository.save(answer);
    }

    @PutMapping("/{id}")
    public Answer updateAnswer(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "id") Long answerId ,@Valid @RequestBody AnswerBody answerDetails){
        Answer answer = answerRepository.findById(answerId).orElseThrow(() -> new ResourceNotFoundException("answer", "id", answerId));
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);
        Question question = questionRepository.findById(answerDetails.getQuestionId()).orElseThrow(() -> new ResourceNotFoundException("question", "id", answerDetails.getQuestionId()));

        boolean canUpdate = false;
        if(role.equals("Administrator") || role.equals("Owner"))
            canUpdate = true;

        if(!canUpdate)
            throw new InvalidAuthorizationException();

        if(answer.getTenant().getId() != validSession.getCustomer().getTenant().getId())
            throw new InvalidAuthorizationException();

        answer.setCorrect(answerDetails.getCorrect());
        answer.setAnswerOrder(answerDetails.getAnswerOrder());
        answer.setAnswerText(answerDetails.getAnswerText());
        answer.setQuestion(question);

        return answerRepository.save(answer);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAnswer(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "id") Long answerId) {
        Answer answer = answerRepository.findById(answerId).orElseThrow(() -> new ResourceNotFoundException("answer", "id", answerId));
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean canUpdate = false;
        if(role.equals("Administrator") || role.equals("Owner"))
            canUpdate = true;

        if(!canUpdate)
            throw new InvalidAuthorizationException();

        if(answer.getTenant().getId() != validSession.getCustomer().getTenant().getId())
            throw new InvalidAuthorizationException();

        answerRepository.delete(answer);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/questions/{id}")
    public List<Answer> getAnswers(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "id") Long questionId) {
        Question question = questionRepository.findById(questionId).orElseThrow(() -> new ResourceNotFoundException("question", "id", questionId));
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean isAdmin = false;
        if(role.equals("Administrator") || role.equals("Owner"))
            isAdmin = true;

        List<Answer> answers = answerRepository.findByQuestion(question);

        return answers;
    }

    @GetMapping("/{id}")
    public Answer getAnswer(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "id") Long answerId) {
        Answer answer = answerRepository.findById(answerId).orElseThrow(() -> new ResourceNotFoundException("answer", "id", answerId));
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean isAdmin = false;
        if(role.equals("Administrator") || role.equals("Owner"))
            isAdmin = true;

        if(answer.getTenant().getId() != validSession.getCustomer().getTenant().getId())
            throw new InvalidAuthorizationException();

        return answer;
    }
}
