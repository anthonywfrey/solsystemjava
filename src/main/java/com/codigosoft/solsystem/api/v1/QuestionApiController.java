/**
 * This is the API controller for the Question model. This will allow for the creating retrieving, updating and deleting
 * the model via REST.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-8-2019
 */
package com.codigosoft.solsystem.api.v1;

import com.codigosoft.solsystem.exceptions.InvalidAuthorizationException;
import com.codigosoft.solsystem.exceptions.ResourceNotFoundException;
import com.codigosoft.solsystem.models.Assignment;
import com.codigosoft.solsystem.models.Question;
import com.codigosoft.solsystem.models.apimodels.QuestionBody;
import com.codigosoft.solsystem.models.apimodels.ValidSession;
import com.codigosoft.solsystem.repositories.AssignmentRepository;
import com.codigosoft.solsystem.repositories.CustomerRepository;
import com.codigosoft.solsystem.repositories.CustomerRoleRepository;
import com.codigosoft.solsystem.repositories.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.ast.Assign;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/questions")
public class QuestionApiController {
    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    AssignmentRepository assignmentRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CustomerRoleRepository customerRoleRepository;

    @PostMapping("")
    public Question createQuestion(@RequestHeader(value = "SessionToken") String sessionToken, @Valid @RequestBody QuestionBody questionDetails) {
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);
        Assignment assignment = assignmentRepository.findById(questionDetails.getAssignmentId()).orElseThrow(() -> new ResourceNotFoundException("assignment", "id", questionDetails.getAssignmentId()));

        boolean canUpdate = false;
        if(role.equals("Administrator") || role.equals("Owner"))
            canUpdate = true;

        if(!canUpdate)
            throw new InvalidAuthorizationException();

        Question question = new Question();
        question.setTenant(validSession.getCustomer().getTenant());
        question.setQuestionText(questionDetails.getQuestionText());
        question.setQuestionOrder(questionDetails.getQuestionOrder());
        question.setAssignment(assignment);

        return questionRepository.save(question);
    }

    @PutMapping("/{id}")
    public Question updateQuestion(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "id") Long questionId, @Valid @RequestBody QuestionBody questionDetails) {
        Question question = questionRepository.findById(questionId).orElseThrow(() -> new ResourceNotFoundException("question", "id", questionId));
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);
        Assignment assignment = assignmentRepository.findById(questionDetails.getAssignmentId()).orElseThrow(() -> new ResourceNotFoundException("assignment", "id", questionDetails.getAssignmentId()));

        boolean canUpdate =false;
        if(role.equals("Administrator") || role.equals("Owner"))
            canUpdate = true;

        if(!canUpdate)
            throw new InvalidAuthorizationException();

        if(question.getTenant().getId() != validSession.getCustomer().getTenant().getId())
            throw new InvalidAuthorizationException();

        question.setQuestionOrder(questionDetails.getQuestionOrder());
        question.setQuestionText(questionDetails.getQuestionText());
        question.setAssignment(assignment);

        return questionRepository.save(question);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteQuestion(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "id") Long questionId) {
        Question question = questionRepository.findById(questionId).orElseThrow(() -> new ResourceNotFoundException("question", "id", questionId));
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean canUpdate = false;
        if(role.equals("Administrator") || role.equals("Owner"))
            canUpdate = true;

        if(!canUpdate)
            throw new InvalidAuthorizationException();

        questionRepository.delete(question);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/assignments/{assignmentid}")
    public List<Question> getQuestions(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "assignmentid") Long assignmentId) {
        Assignment assignment = assignmentRepository.findById(assignmentId).orElseThrow(() -> new ResourceNotFoundException("assignment", "id", assignmentId));
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean isAdmin = false;
        if(role.equals("Administrator") || role.equals("Owner"))
            isAdmin = true;

        if(assignment.getTenant().getId() != validSession.getCustomer().getTenant().getId())
            throw new InvalidAuthorizationException();

        List<Question> questions = questionRepository.findByAssignment(assignment);
        return questions;
    }

    @GetMapping("/{id}")
    public Question getQuestion(@RequestHeader("SessionToken") String sessionToken, @PathVariable(name = "id") Long questionId) {
        Question question = questionRepository.findById(questionId).orElseThrow(() -> new ResourceNotFoundException("question", "id", questionId));
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean isAdmin = false;
        if(role.equals("Administrator") || role.equals("Owner"))
            isAdmin = true;

        if(question.getTenant().getId() != validSession.getCustomer().getTenant().getId())
            throw new InvalidAuthorizationException();
        return question;
    }
}
