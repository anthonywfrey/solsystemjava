/**
 * This is the API controller for the Section model. This will allow for the creating retrieving, updating and deleting
 * the model via REST.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-4-2019
 */
package com.codigosoft.solsystem.api.v1;

import com.codigosoft.solsystem.exceptions.InvalidAuthorizationException;
import com.codigosoft.solsystem.exceptions.ResourceNotFoundException;
import com.codigosoft.solsystem.models.Classroom;
import com.codigosoft.solsystem.models.Section;
import com.codigosoft.solsystem.models.Tenant;
import com.codigosoft.solsystem.models.apimodels.SectionBody;
import com.codigosoft.solsystem.models.apimodels.ValidSession;
import com.codigosoft.solsystem.repositories.ClassroomRepository;
import com.codigosoft.solsystem.repositories.CustomerRepository;
import com.codigosoft.solsystem.repositories.CustomerRoleRepository;
import com.codigosoft.solsystem.repositories.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/sections")
public class SectionApiController {
    @Autowired
    SectionRepository sectionRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CustomerRoleRepository customerRoleRepository;

    @Autowired
    ClassroomRepository classroomRepository;

    @PostMapping("")
    public Section createSection(@RequestHeader(value = "SessionToken") String sessionToken, @Valid @RequestBody SectionBody sectionDetails) {
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);
        Classroom classroom = classroomRepository.findById(sectionDetails.getClassroomId()).orElseThrow(() -> new ResourceNotFoundException("classroom", "id", sectionDetails.getClassroomId()));

        boolean canUpdate = false;
        if(role.equals("Owner") || role.equals("Administrator"))
            canUpdate = true;

        if(!canUpdate)
            throw new InvalidAuthorizationException();

        Section section = new Section();
        section.setName(sectionDetails.getName());
        section.setSectionOrder(sectionDetails.getSectionOrder());
        section.setTenant(validSession.getCustomer().getTenant());
        section.setClassroom(classroom);
        section.setTenant(validSession.getCustomer().getTenant());
        return sectionRepository.save(section);
    }

    @PutMapping("/{id}")
    public Section updateSection(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "id") Long sectionId, @Valid @RequestBody SectionBody sectionDetails) {
        Section section = sectionRepository.findById(sectionId).orElseThrow(() -> new ResourceNotFoundException("section", "id", sectionId));
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);
        Classroom classroom = classroomRepository.findById(sectionDetails.getClassroomId()).orElseThrow(() -> new ResourceNotFoundException("classroom", "id", sectionDetails.getClassroomId()));

        boolean canUpdate = false;
        if(role.equals("Owner") || role.equals("Administrator"))
            canUpdate = true;

        if(!canUpdate)
            throw new InvalidAuthorizationException();

        if(section.getTenant().getId() != validSession.getCustomer().getTenant().getId())
            throw new InvalidAuthorizationException();

        section.setClassroom(classroom);
        section.setName(sectionDetails.getName());
        section.setSectionOrder(sectionDetails.getSectionOrder());

        Section updated = sectionRepository.save(section);

        return updated;
    }

    @GetMapping("/classrooms/{classroomid}")
    public List<Section> getSections(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "classroomid") Long classroomId) {
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        Classroom classroom = classroomRepository.findById(classroomId).orElseThrow(() -> new ResourceNotFoundException("classroom", "id", classroomId));
        String role =validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean isAdmin = false;
        if(role.equals("Administrator") || role.equals("Owner"))
            isAdmin = true;


        List<Section> sections = sectionRepository.findByClassroom(classroom);
        return sections;
    }

    @GetMapping("/{sectionid}")
    public Section getSection(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "sectionid") Long sectionId) {
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role =validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean isAdmin = false;
        if(role.equals("Administrator") || role.equals("Owner"))
            isAdmin = true;

        Section section = sectionRepository.findById(sectionId).orElseThrow(()-> new ResourceNotFoundException("section", "id", sectionId));

        if(section.getTenant().getId() != validSession.getCustomer().getTenant().getId()) {
            throw new InvalidAuthorizationException();
        }

        return section;
    }

    @DeleteMapping("/{sectionid}")
    public ResponseEntity<?> deleteSection(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "sectionid") Long sectionId) {
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role =validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean isAdmin = false;
        if(role.equals("Administrator") || role.equals("Owner"))
            isAdmin = true;

        if(!isAdmin)
            throw new InvalidAuthorizationException();

        Section section = sectionRepository.findById(sectionId).orElseThrow(()-> new ResourceNotFoundException("section", "id", sectionId));

        if(section.getTenant().getId() != validSession.getCustomer().getTenant().getId()) {
            throw new InvalidAuthorizationException();
        }

        sectionRepository.delete(section);
        return ResponseEntity.ok().build();
    }
}
