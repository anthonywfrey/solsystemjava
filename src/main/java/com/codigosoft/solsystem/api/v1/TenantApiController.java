/**
 * This is the API Controller for version 1 of the Tenants Enttity. It will is used for the basic CRUD operations
 * for basic use alongside external applications.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-7-2019
 */
package com.codigosoft.solsystem.api.v1;


import com.codigosoft.solsystem.exceptions.ResourceNotFoundException;
import com.codigosoft.solsystem.models.Tenant;
import com.codigosoft.solsystem.repositories.TenantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/tenants")
public class TenantApiController {
    @Autowired
    TenantRepository tenantRepository;

    //Get all tenants
    @GetMapping("")
    public List<Tenant> getAllTenants() {
        return tenantRepository.findAll();
    }
    //Get tenant by id
    @GetMapping("/{id}")
    public Tenant getTenantById(@PathVariable(value = "id") Long tenantId) {
        return tenantRepository.findById(tenantId).orElseThrow(() -> new ResourceNotFoundException("Tenant", "id",  tenantId));
    }

    //create tenant
    @PostMapping("")
    public Tenant createTenant(@Valid @RequestBody Tenant tenant) {
        return tenantRepository.save(tenant);
    }

    //update tenant
    @PutMapping("/{id}")
    public Tenant updateTenant(@PathVariable(value = "id") Long tenantId, @Valid @RequestBody Tenant tenantDetails) {
        Tenant tenant = tenantRepository.findById(tenantId).orElseThrow(() -> new ResourceNotFoundException("tenant", "id", tenantId));

        tenant.setName(tenantDetails.getName());

        Tenant updated = tenantRepository.save(tenant);
        return updated;
    }

    //delete tenant
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTenant(@PathVariable(value = "id") Long tenantId) {
        Tenant tenant = tenantRepository.findById(tenantId).orElseThrow(() -> new ResourceNotFoundException("tenant", "id", tenantId));

        tenantRepository.delete(tenant);

        return ResponseEntity.ok().build();
    }
}
