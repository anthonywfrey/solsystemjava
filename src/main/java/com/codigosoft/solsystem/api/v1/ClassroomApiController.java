/**
 * This is the API controller for the Classroom model. This will allow for the creating retrieving, updating and deleting
 * the model via REST.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-4-2019
 */
package com.codigosoft.solsystem.api.v1;

import com.codigosoft.solsystem.exceptions.InvalidAuthorizationException;
import com.codigosoft.solsystem.exceptions.ResourceNotFoundException;
import com.codigosoft.solsystem.models.Classroom;
import com.codigosoft.solsystem.models.Enrollment;
import com.codigosoft.solsystem.models.apimodels.ValidSession;
import com.codigosoft.solsystem.repositories.ClassroomRepository;
import com.codigosoft.solsystem.repositories.CustomerRepository;
import com.codigosoft.solsystem.repositories.CustomerRoleRepository;
import com.codigosoft.solsystem.repositories.EnrollmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/classrooms")
public class ClassroomApiController {
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    ClassroomRepository classroomRepository;

    @Autowired
    CustomerRoleRepository customerRoleRepository;

    @Autowired
    EnrollmentRepository enrollmentRepository;

    @GetMapping("")
    public List<Classroom> getClassrooms(@RequestHeader(value = "SessionToken") String sessionToken) {
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String customerRole = validSession.retrieveCustomerRoleName(customerRoleRepository);
        List<Classroom> classrooms;
        if(customerRole.equals("Owner") || customerRole.equals("Administrator")) {
            classrooms = classroomRepository.findByTenant(validSession.getCustomer().getTenant());
        } else
        {
            classrooms = classroomRepository.findByCustomerEnrollment(validSession.getCustomer().getId());
        }

        return classrooms;
    }

    @GetMapping("/{classroomid}")
    public Classroom getClassroom(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "classroomid") Long classroomId) {
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        Classroom classroom = classroomRepository.findById(classroomId).orElseThrow(() -> new ResourceNotFoundException("classroom", "id", classroomId));
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        if(validSession.getCustomer().getTenant().getId() != classroom.getTenant().getId()) {
            throw new ResourceNotFoundException("classroom", "id", classroomId);
        }

        if(role.equals("Owner") || role.equals("Administrator")) {
            return classroom;
        }

        List<Enrollment> enrollment = enrollmentRepository.findByCustomerAndClassroom(validSession.getCustomer(), classroom);

        if(enrollment.size() <= 0) {
            throw new ResourceNotFoundException("classroom", "id", classroomId);
        }
        return classroom;
    }

    @PostMapping("")
    public Classroom insertClassroom(@RequestHeader(value = "SessionToken") String sessionToken, @Valid @RequestBody Classroom classroom) {
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean canUpdate = false;
        if(role.equals("Owner") || role.equals("Administrator")) {
            canUpdate = true;
        }

        if(!canUpdate)
            throw new InvalidAuthorizationException();

        classroom.setTenant(validSession.getCustomer().getTenant());

        Classroom newClassroom = classroomRepository.save(classroom);
        return newClassroom;
    }

    @PutMapping("/{classroomid}")
    public Classroom updateClassroom(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "classroomid") Long classroomId, @Valid @RequestBody Classroom classroomDetails) {
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        Classroom classroom = classroomRepository.findById(classroomId).orElseThrow(() -> new ResourceNotFoundException("classroom", "id", classroomId));
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        if(validSession.getCustomer().getTenant().getId() != classroom.getTenant().getId()) {
            throw new ResourceNotFoundException("classroom", "id", classroomId);
        }

        boolean canUpdate = false;
        if(role.equals("Owner") || role.equals("Administrator")) {
            canUpdate = true;
        }

        if(!canUpdate)
            throw new InvalidAuthorizationException();

        classroom.setName(classroomDetails.getName());
        classroom.setDescription(classroomDetails.getDescription());
        classroom.setStartDate(classroomDetails.getStartDate());
        classroom.setEndDate(classroomDetails.getEndDate());

        Classroom updated = classroomRepository.save(classroom);

        return classroom;
    }

    @DeleteMapping("/{classroomid}")
    public ResponseEntity<?> deleteClassroom(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "classroomid") Long classroomId) {
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        Classroom classroom = classroomRepository.findById(classroomId).orElseThrow(() -> new ResourceNotFoundException("classroom", "id", classroomId));
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        if(validSession.getCustomer().getTenant().getId() != classroom.getTenant().getId()) {
            throw new ResourceNotFoundException("classroom", "id", classroomId);
        }

        boolean canDelete = false;
        if(role.equals("Owner") || role.equals("Administrator")) {
            canDelete = true;
        }

        if(!canDelete)
            throw new InvalidAuthorizationException();

        classroomRepository.delete(classroom);
        return ResponseEntity.ok().build();
    }


}
