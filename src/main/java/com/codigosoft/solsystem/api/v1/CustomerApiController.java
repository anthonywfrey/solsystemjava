/**
 * This is the API Controller for version 1 of the Customer Enttity. It will is used for the basic CRUD operations
 * for basic use alongside external applications.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-7-2019
 */
package com.codigosoft.solsystem.api.v1;

import com.codigosoft.solsystem.exceptions.ResourceNotFoundException;
import com.codigosoft.solsystem.models.Customer;
import com.codigosoft.solsystem.models.NewCustomer;
import com.codigosoft.solsystem.models.Tenant;
import com.codigosoft.solsystem.repositories.CustomerRepository;
import com.codigosoft.solsystem.repositories.TenantRepository;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerApiController {
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    TenantRepository tenantRepository;

    //Get all customers
    @GetMapping("")
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }
    //Get one customer by id
    @GetMapping("/{id}")
    public Customer getCustomerById(@PathVariable(value = "id") Long customerId) {
        return customerRepository.findById(customerId).orElseThrow(() -> new ResourceNotFoundException("customer", "id", customerId));
    }

    //Insert customer
    @PostMapping("")
    public Customer insetCustomer(@Valid @RequestBody Customer customer) {
        return customerRepository.save(customer);
    }

    //update customer
    @PutMapping("/{id}")
    public Customer updateCustomer(@PathVariable(value = "id") Long customerId, @Valid @RequestBody Customer customerDetails) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(() -> new ResourceNotFoundException("customer", "id", customerId));

        customer.setEmail(customerDetails.getEmail());
        customer.setPassword(customerDetails.getPassword());

        Customer updated = customerRepository.save(customer);

        return updated;
    }

    //delete customer

    //Test endpoint for password

    @PostMapping("/test")
    public HashMap<String, String> testPassword(@Valid @RequestBody NewCustomer customerDetails) {
        HashMap<String, String> updated = new HashMap<>();

        updated.put("UpdatedPassword", customerDetails.getPassword());
        return updated;
    }

    @PostMapping("/testpass")
    public HashMap<String, String> testPasswordCompare(@Valid @RequestBody NewCustomer customerDetails) {
        HashMap<String, String> updated = new HashMap<>();
        Tenant tenant = tenantRepository.findById(customerDetails.getTenantId()).orElse(new Tenant());

        //updated.put("TEST", customerDetails.getTenantId().toString());
        //return updated;

        if (tenant.getId() == null)
        {
            updated.put("Error", "The tenant was not found");
            return updated;
        }


        List<Customer> customers = customerRepository.findByEmailAndTenant(customerDetails.getEmail(), tenant);

        if (customers.size() <= 0) {
            updated.put("Error", "customer not found");
            return updated;
        }

        Customer customer = customers.get(0);
        boolean test = BCrypt.checkpw(customerDetails.getPassword(), customer.getPasswordDigest());

        if (!BCrypt.checkpw(customerDetails.getPassword(), customer.getPasswordDigest()))
        {
            updated.put("Error", "Password doesn't match");
           // updated.put("Success", "Password Matched");
        }
        else {
            updated.put("Error", "Password doesn't match");
        }

        return updated;
    }
}
