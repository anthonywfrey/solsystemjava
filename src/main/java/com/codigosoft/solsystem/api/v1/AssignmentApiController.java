/**
 * This is the API controller for the Assignment model. This will allow for the creating retrieving, updating and deleting
 * the model via REST.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 4-8-2019
 */
package com.codigosoft.solsystem.api.v1;

import com.codigosoft.solsystem.exceptions.InvalidAuthorizationException;
import com.codigosoft.solsystem.exceptions.ResourceNotFoundException;
import com.codigosoft.solsystem.models.Assignment;
import com.codigosoft.solsystem.models.Section;
import com.codigosoft.solsystem.models.apimodels.AssignmentBody;
import com.codigosoft.solsystem.models.apimodels.ValidSession;
import com.codigosoft.solsystem.repositories.AssignmentRepository;
import com.codigosoft.solsystem.repositories.CustomerRepository;
import com.codigosoft.solsystem.repositories.CustomerRoleRepository;
import com.codigosoft.solsystem.repositories.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/assignments")
public class AssignmentApiController {
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CustomerRoleRepository customerRoleRepository;

    @Autowired
    SectionRepository sectionRepository;

    @Autowired
    AssignmentRepository assignmentRepository;

    @PostMapping("")
    public Assignment createAssignment(@RequestHeader(value = "SessionToken") String sessionToken, @Valid @RequestBody AssignmentBody assignmentDetails) {
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);
        Section section = sectionRepository.findById(assignmentDetails.getSectionId()).orElseThrow(() -> new ResourceNotFoundException("section", "id", assignmentDetails.getSectionId()));

        boolean canUpdate = false;
        if(role.equals("Owner") || role.equals("Administrator"))
            canUpdate = true;

        if(!canUpdate)
            throw new InvalidAuthorizationException();

        Assignment assignment = new Assignment();
        assignment.setName(assignmentDetails.getName());
        assignment.setContent(assignmentDetails.getContent());
        assignment.setAssignmentOrder(assignmentDetails.getAssignmentOrder());
        assignment.setMaxPoints(assignmentDetails.getMaxPoints());
        assignment.setTenant(validSession.getCustomer().getTenant());
        assignment.setIsGraded(assignmentDetails.getIsGraded());
        assignment.setSection(section);

        assignment.setTenant(validSession.getCustomer().getTenant());
        return assignmentRepository.save(assignment);
    }

    @PutMapping("/{id}")
    public Assignment updateAssignment(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "id") Long assignmentId, @Valid @RequestBody AssignmentBody assignmentDetails) {
        Assignment assignment = assignmentRepository.findById(assignmentId).orElseThrow(()-> new ResourceNotFoundException("assignment", "id", assignmentId));
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);
        Section section = sectionRepository.findById(assignmentDetails.getSectionId()).orElseThrow(() -> new ResourceNotFoundException("section", "id", assignmentDetails.getSectionId()));

        boolean canUpdate = false;
        if(role.equals("Owner") || role.equals("Administrator"))
            canUpdate = true;

        if(!canUpdate)
            throw new InvalidAuthorizationException();

        if(assignment.getTenant().getId() != validSession.getCustomer().getTenant().getId())
            throw new InvalidAuthorizationException();

        assignment.setMaxPoints(assignmentDetails.getMaxPoints());
        assignment.setIsGraded(assignmentDetails.getIsGraded());
        assignment.setAssignmentOrder(assignmentDetails.getAssignmentOrder());
        assignment.setName(assignmentDetails.getName());
        assignment.setContent(assignmentDetails.getContent());
        assignment.setSection(section);

        return assignmentRepository.save(assignment);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAssignment(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "id") Long assignmentId) {
        Assignment assignment = assignmentRepository.findById(assignmentId).orElseThrow(() -> new ResourceNotFoundException("assignment", "id", assignmentId));
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean canUpdate = false;
        if(role.equals("Owner") || role.equals("Administrator"))
            canUpdate = true;

        if(!canUpdate)
            throw new InvalidAuthorizationException();

        if(assignment.getTenant().getId() != validSession.getCustomer().getTenant().getId())
            throw new InvalidAuthorizationException();

        assignmentRepository.delete(assignment);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/sections/{sectionid}")
    public List<Assignment> getAssignments(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "sectionid") Long sectionId) {
        Section section = sectionRepository.findById(sectionId).orElseThrow(() -> new ResourceNotFoundException("section", "id", sectionId));
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean isAdmin = false;
        if(role.equals("Owner") || role.equals("Administrator"))
            isAdmin = true;

        List<Assignment> assignments = assignmentRepository.findBySection(section);

        return assignments;
    }

    @GetMapping("/{id}")
    public Assignment getAssignment(@RequestHeader(value = "SessionToken") String sessionToken, @PathVariable(name = "id") Long assignmentId) {
        Assignment assignment = assignmentRepository.findById(assignmentId).orElseThrow(() -> new ResourceNotFoundException("assignment", "id", assignmentId));
        ValidSession validSession = new ValidSession(sessionToken, customerRepository);
        String role = validSession.retrieveCustomerRoleName(customerRoleRepository);

        boolean isAdmin = false;
        if(role.equals("Owner") || role.equals("Administrator"))
            isAdmin = true;

        if(assignment.getTenant().getId() != validSession.getCustomer().getTenant().getId())
            throw new InvalidAuthorizationException();


        return assignment;
    }
}
