/**
 * This is the customer role entity repository for use with JPA and Hibernate.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-11-2019
 */
package com.codigosoft.solsystem.repositories;

import com.codigosoft.solsystem.models.Customer;
import com.codigosoft.solsystem.models.CustomerRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRoleRepository extends JpaRepository<CustomerRole, Long> {
    public List<CustomerRole> findByCustomer(Customer customer);
}
