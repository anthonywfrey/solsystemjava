/**
 * This is the classroom entity repository for use with JPA and Hibernate.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-13-2019
 */
package com.codigosoft.solsystem.repositories;

import com.codigosoft.solsystem.models.Classroom;
import com.codigosoft.solsystem.models.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClassroomRepository extends JpaRepository<Classroom, Long> {
    public List<Classroom> findByTenant(Tenant tenant);

    @Query(value="Select c.* from classrooms c join enrollments e on e.classroom_id = c.id where e.customer_id = :customer_id", nativeQuery = true)
    public List<Classroom> findByCustomerEnrollment(@Param("customer_id") Long customerId);
}
