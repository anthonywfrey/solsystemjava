/**
 * This is the answer entity repository for use with JPA and Hibernate.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-13-2019
 */
package com.codigosoft.solsystem.repositories;

import com.codigosoft.solsystem.models.Answer;
import com.codigosoft.solsystem.models.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AnswerRepository extends JpaRepository<Answer, Long> {
    List<Answer> findByQuestion(Question question);

    @Query(value = "select a.* from answers a join questions q on q.id = a.question_id join assignments asg on asg.id = q.assignment_id where asg.id = :assignment_id", nativeQuery = true)
    List<Answer> findByAssignmentId(@Param(value = "assignment_id") Long assignmentId);
}
