/**
 * This is the enrollment entity repository for use with JPA and Hibernate.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-29-2019
 */
package com.codigosoft.solsystem.repositories;

import com.codigosoft.solsystem.models.Classroom;
import com.codigosoft.solsystem.models.Customer;
import com.codigosoft.solsystem.models.Enrollment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EnrollmentRepository extends JpaRepository<Enrollment, Long> {
    List<Enrollment> findByClassroom(Classroom classroom);
    List<Enrollment> findByCustomer(Customer customer);
    List<Enrollment> findByCustomerAndClassroom(Customer customer, Classroom classroom);
}
