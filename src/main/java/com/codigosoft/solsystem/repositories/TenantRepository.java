/**
 * This is the Tenant entity repository for use with JPA and Hibernate.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-7-2019
 */
package com.codigosoft.solsystem.repositories;

import com.codigosoft.solsystem.models.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TenantRepository extends JpaRepository<Tenant, Long> {
    List<Tenant> findByName(String name);
    List<Tenant> findBySlug(String slug);
    List<Tenant> findByNameOrSlug(String name, String slug);
}
