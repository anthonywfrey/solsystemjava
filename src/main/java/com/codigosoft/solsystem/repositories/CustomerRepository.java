/**
 * This is the Customer entity repository for use with JPA and Hibernate.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-7-2019
 */
package com.codigosoft.solsystem.repositories;

import com.codigosoft.solsystem.models.Customer;
import com.codigosoft.solsystem.models.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    List<Customer> findByEmailAndTenant(String email, Tenant tenant);
    List<Customer> findByTenant(Tenant tenant);
    List<Customer> findBySessionToken(String sessionToken);

    @Query(value = "SELECT c.* FROM customers c join customer_roles cr on cr.customer_id = c.id join roles r on r.id = cr.role_id where r.role_name = 'Instructor'", nativeQuery = true)
    List<Customer> findInstructors();

    @Query(value = "SELECT c.* FROM customers c join customer_roles cr on cr.customer_id = c.id join roles r on r.id = cr.role_id where r.role_name = 'Student'", nativeQuery = true)
    List<Customer> findStudents();

    @Query(value="select c.* from customers c join enrollments e on e.customer_id = c.id where e.classroom_id = :classroom_id", nativeQuery = true)
    List<Customer> findByClassroomEnrollment(@Param("classroom_id") Long classroomId);

    @Query(value="select c.* from customers c join enrollments e on e.customer_id = c.id join customer_roles cr on cr.customer_id = c.id join roles r on r.id=cr.role_id where r.role_name = 'Student' and  e.classroom_id = :classroom_id", nativeQuery = true)
    List<Customer> findStudentClassroomEnrollment(@Param("classroom_id") Long classroomId);
}
