/**
 * This is the question entity repository for use with JPA and Hibernate.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-14-2019
 */
package com.codigosoft.solsystem.repositories;

import com.codigosoft.solsystem.models.Assignment;
import com.codigosoft.solsystem.models.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface QuestionRepository extends JpaRepository<Question, Long> {
    List<Question> findByAssignment(Assignment assignment);

    @Query(value = "Select * from questions where assignment_id = :assignment_id", nativeQuery = true)
    List<Question> findByAssignmentId(@Param(value = "assignment_id") Long assignmentId);
}
