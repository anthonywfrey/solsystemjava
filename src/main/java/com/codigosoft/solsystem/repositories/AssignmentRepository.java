/**
 * This is the assignment entity repository for use with JPA and Hibernate.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-14-2019
 */
package com.codigosoft.solsystem.repositories;

import com.codigosoft.solsystem.models.Assignment;
import com.codigosoft.solsystem.models.Section;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AssignmentRepository extends JpaRepository<Assignment, Long> {
    List<Assignment> findBySection(Section section);
}
