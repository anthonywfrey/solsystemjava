/**
 * This is the section entity repository for use with JPA and Hibernate.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-13-2019
 */
package com.codigosoft.solsystem.repositories;

import com.codigosoft.solsystem.models.Classroom;
import com.codigosoft.solsystem.models.Section;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SectionRepository extends JpaRepository<Section, Long> {
    public List<Section> findByClassroom(Classroom classroom);
}
