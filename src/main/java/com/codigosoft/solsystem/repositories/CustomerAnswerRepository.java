/**
 * This is the customer answer entity repository for use with JPA and Hibernate.
 *
 * @author: Anthony Frey - anthony.frey@snhu.edu
 * @since: 3-29-2019
 */
package com.codigosoft.solsystem.repositories;

import com.codigosoft.solsystem.models.CustomerAnswer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CustomerAnswerRepository extends JpaRepository<CustomerAnswer, Long> {

    @Query(value = "select ca.* from customer_answers ca join questions q on q.id = ca.question_id join assignments a on a.id = q.assignment_id where ca.customer_id = :customer_id and a.id = :assignment_id order by ca.question_id", nativeQuery = true)
    List<CustomerAnswer> findByCustomerIdAndAssignmentId(@Param("customer_id") Long customerId, @Param("assignment_id") Long assignmentId);
}
