SolSystem
=========

## Code for my simple online learning system prototype meant for IT-700

This is the code repository for my IT-700 project, it will remain public for the duration of the class plus a few weeks minimum.
This project has been developed with Java and the Spring Boot Project, all code outside of the framework and included libraries
has been coded by me.

To run this project, you will need at least **Java 8 and Maven**. The command to compile and start the server, simply type the command **mvn spring-boot:run** from the root directory
of the project.